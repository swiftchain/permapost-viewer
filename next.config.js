module.exports = {
  target: "serverless",
  reactStrictMode: true,
  env: {
    //ETH_RPC: "https://d8a8054f0e9440f68e34f7ab44a78b09.eth.rpc.rivet.cloud/",
    ETH_RPC: "https://cloudflare-eth.com/",
    ETH_CONTRACT_ADDRESS: "0x8D213dc5Cc6c3D2746E2B90a2D1C9E589ebCb532",
    ETH_TnC_CONTRACT_ADDRESS: "0xa3A9664532E063A9D8188B515ef7F1BcB8583bBD",
    // XDAI_RPC: "https://rpc.xdaichain.com/",
    XDAI_RPC: "https://xdai-archive.blockscout.com/",
    XDAI_CONTRACT_ADDRESS: "0x2024A680EE628725bd99c36Df223189f678C6945",
    XDAI_TnC_CONTRACT_ADDRESS: "0xF7cF1eA71FEc419900eBde53B66B7C8435774Fd7",
    BLOCKCHAINS: {
      xdai: "xDai",
      eth: "Ethereum",
      //connected: "Connected Wallet",
    },
  },
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    config.resolve.extensions.push(".elm");
    config.module.rules.push({
      test: /\.elm$/,
      exclude: [/elm-stuff/, /node_modules/],
      use: {
        loader: "elm-webpack-loader",
        options: {
          // pathToElm: 'node_modules/.bin/elm'
        },
      },
    });
    return config;
  },
};
