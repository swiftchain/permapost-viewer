# Run development build:

npm run dev

# Install local blockchain app:

npm install -g ganache-cli

# Run local blockchain:

./scripts/fork-local-node.sh

# Local blockchain test account private key:

0x50ae4f06ab6648ee11d98746f862f747d79ab42363637a6d09f3685d2952cec5

# Test wallet

Metamask
