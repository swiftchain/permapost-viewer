const copyToClipBoard = (value) => {
  try {
    const el = document.createElement("input");
    el.value = value;
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
    return true;
  } catch (error) {
    return false;
  }
};

export default copyToClipBoard;
