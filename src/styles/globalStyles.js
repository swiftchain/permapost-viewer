const GlobalStyles = `
* {
    box-sizing: border-box;
}
html, body {
    padding: 0;
    margin: 0;
    font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;
    font-size: 114%;
    line-height: 1.4285em;
    color: rgba(0,0,0,.87);
}
body.true {
    overflow: hidden;
}
@media
only screen and (-webkit-min-device-pixel-ratio: 1.25),
only screen and ( min--moz-device-pixel-ratio: 1.25),
only screen and ( -o-min-device-pixel-ratio: 1.25/1),
only screen and ( min-device-pixel-ratio: 1.25),
only screen and ( min-resolution: 200dpi),
only screen and ( min-resolution: 1.25dppx)
{
    html, body {
        font-size: 21px;
    }
}
label {
    display: block;
    margin: 0 0 .28571429rem 0;
    color: rgba(0,0,0,.87);
    font-size: .92857143em;
    font-weight: 700;
    text-transform: none;
}

input {
    -webkit-text-size-adjust: 100%;
    -webkit-tap-highlight-color: transparent;
    box-sizing: inherit;
    overflow: visible;
    width: 100%;
    vertical-align: top;
    margin: 0;
    outline: 0;
    -webkit-appearance: none;
    line-height: 1.21428571em;
    padding: .67857143em 1em;
    font-size: 1em;
    background: #fff;
    border: 0.071em solid rgba(34,36,38,.15);
    color: rgba(0,0,0,.87);
    border-radius: .28571429rem;
    box-shadow: 0 0 0 0 transparent inset;
    -webkit-transition: all 0.15s ease;
    transition: all 0.15s ease;
    &:hover {
        border-color: #808083;
    }
    &:focus {
        border-color: #000000;
    }
    &:not(:focus):not(:placeholder-shown):valid{
        border-color: #2c662d;
    }
}

textarea {
    overflow: auto;
    width: 100%;
    margin: 0;
    -webkit-appearance: none;
    padding: .78em 1em;
    background: #fff;
    border: 0.071em solid rgba(34,36,38,.15);
    outline: 0;
    color: rgba(0,0,0,.87);
    border-radius: .28rem;
    box-shadow: 0 0 0 0 transparent inset;
    font-size: 1em;
    line-height: 1.2857;
    resize: none;
    vertical-align: top;
    -webkit-transition: all 0.15s ease;
    transition: all 0.15s ease;
    &:hover {
        border-color: #808083;
    }
    &:focus {
        border-color: #000000;
    }
    &:not(:focus):not(:placeholder-shown):valid{
        border-color: #2c662d;
    }
}

select {
    padding: .67857143em 2.1em .67857143em 1em;
    border: 0.071em solid rgba(34,36,38,.15);
    border-radius: .28571429rem;
    -webkit-transition: all 0.15s ease;
    transition: all 0.15s ease;
    &:hover {
        border-color: #808083;
    }
    &:focus {
        border-color: #000000;
    }
    &:not(:focus):not(:placeholder-shown):valid{
        border-color: #2c662d;
    }
}

h1 {
    -webkit-text-size-adjust: 100%;
    -webkit-tap-highlight-color: transparent;
    box-sizing: inherit;
    min-height: 1rem;
    border: none;
    margin: calc(2rem - .14285714em) 0 1rem;
    padding: 0 0;
    font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;
    font-weight: 700;
    line-height: 1.28571429em;
    text-transform: none;
    color: rgba(0,0,0,.87);
    font-size: 2rem;
}

h2 {
    -webkit-text-size-adjust: 100%;
    -webkit-tap-highlight-color: transparent;
    border: none;
    margin: calc(2rem - .14285714em) 0 1rem;
    padding: 0 0;
    font-weight: 700;
    line-height: 1.28571429em;
    text-transform: none;
    color: rgba(0,0,0,.87);
    font-size: 1.71428571rem;
}

h3 {
    -webkit-text-size-adjust: 100%;
    -webkit-tap-highlight-color: transparent;
    border: none;
    margin: calc(2rem - .14285714em) 0 1rem;
    padding: 0 0;
    font-weight: 700;
    line-height: 1.28571429em;
    text-transform: none;
    color: rgba(0,0,0,.87);
    font-size: 1.28571429rem;
}

h4 {
    -webkit-text-size-adjust: 100%;
    -webkit-tap-highlight-color: transparent;
    border: none;
    margin: calc(2rem - .14285714em) 0 1rem;
    padding: 0 0;
    font-weight: 700;
    line-height: 1.28571429em;
    text-transform: none;
    color: rgba(0,0,0,.87);
    font-size: 1.07142857rem;
}

h5 {
    -webkit-text-size-adjust: 100%;
    -webkit-tap-highlight-color: transparent;
    border: none;
    margin: calc(2rem - .14285714em) 0 1rem;
    padding: 0 0;
    font-weight: 700;
    line-height: 1.28571429em;
    font-size: 1rem;
}

strong {
    font-weight: bold;
}

p {
    -webkit-text-size-adjust: 100%;
    line-height: 1.4285em;
    margin: 1em 0em;
    margin-top: 0;
}

a {
    color: #000000;
    text-decoration: underline;
    font-weight: bold;
}

button {
    cursor: pointer;
    display: block;
    min-height: 1em;
    outline: 0;
    border: none;
    margin: 0 .25em 0 0;
    padding: .78em 1.5em .78em;
    text-transform: none;
    font-weight: bold;
    line-height: 1em;
    font-style: normal;
    text-align: center;
    text-decoration: none;
    border-radius: .25rem;
    font-size: 1rem;
    background: #ffffff;
    box-shadow: inset 0 0 0 35.7142em #1b1c1d;
    color: #fff;
    transition: all 0.15s ease;
    &:hover {
        background: #ffffff;
        box-shadow: inset 0 0 0 0.142em #1b1c1d;
        color: #1b1c1d;
    }
}

.clickable {
    cursor: pointer;
    &-inactive {
        pointer-events: none;
        background: #a6a7aa;
        box-shadow: none;
    }
}

.text-left {
    text-align: left;
}
.text-center {
    text-align: center;
}
.text-right {
    text-align: right;
}

@media screen and (max-width: 650px) {
    html, body {
        font-size: 100%;
    }
    button {
        width: 100%;
    }
    .hidden-xs {
        display: none;
    }
}
`;

export default GlobalStyles;
