port module PhaceElement exposing (..)

import Browser
import Html exposing (Html, button, div, span)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import Json.Decode as D exposing (Decoder)
import Json.Decode.Pipeline exposing (required)
import Json.Encode as E exposing (Value)

import Element exposing (Element, el, text, row, alignRight, fill, width, rgb255, spacing, centerY, padding)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font

import Eth.Types exposing (Address)
import Eth.Utils
import Phace



-- Ports (interop with props)

port onChange : Int -> Cmd msg


-- Flags (initialization)


type alias Flags =
    { messageFlag : String
    }


flagsDecoder : Decoder Flags
flagsDecoder =
    D.succeed Flags
        |> required "messageFlag" D.string


defaultFlags : Flags
defaultFlags =
    { messageFlag = "0x93fe7d1d24be7cb33329800ba2166f4d28eaa553" }



-- Model

type alias Model =
    { message : String
    }


init : Value -> ( Model, Cmd Msg )
init flagsJson =
    let
        flags =
            D.decodeValue flagsDecoder flagsJson |> Result.withDefault defaultFlags
    in
    ( { message = flags.messageFlag
      }
    , Cmd.none
    )



-- Msg

type Msg
    = Increment
    | Decrement


-- Subscriptions

subscriptions : Model -> Sub Msg
subscriptions _ =
  Sub.none



-- Update

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of

        Increment ->
            ( model
            , onChange 1
            )

        Decrement ->
            ( model
            , onChange -1
            )


main : Program Value Model Msg
main =
    Browser.element
        { view = view
        , update = update
        , init = init
        , subscriptions = subscriptions
        }

view : Model -> Html msg
view model =
    Element.layout [] <|
        row []
        [ 
            el[] <|
                Element.html <|
                (phaceElement 70 (Eth.Utils.unsafeToAddress model.message))
        ]


phaceElement : Int -> Address -> (Html msg)
phaceElement size fromAddress =
    Phace.fromEthAddress fromAddress size size