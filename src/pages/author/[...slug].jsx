import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import styled, { keyframes } from "styled-components";
import { set } from "js-cookie";
import Layout from "./../../components/Layout";
import useScript from "../../lib/useScript";
import PhaceElement from "./../../components/PhaceElement";
import AuthorPost from "./../../components/AuthorPost";
import ReactMarkdown from "react-markdown";
import gfm from "remark-gfm";

const abiDecoder = require("abi-decoder");
const ABI = require("../../lib/abi_2021_02_25.json");
abiDecoder.addABI(ABI);

const Author = ({ show, children = null, hideModal }) => {
  const router = useRouter();
  const [ethWeb3, setEthWeb3] = useState(null);
  const [xDaiWeb3, setXDaiEthWeb3] = useState(null);
  const [searchResults, setSearchResults] = useState();
  const [userAddress, setUserAddress] = useState("");
  const [titleArray, setTitleArray] = useState([""]);
  const [bodyArray, setBodyArray] = useState([]);
  const [postArray, setPostArray] = useState([]);
  const [posts, setPosts] = useState([]);
  const [fetchingStatus, setFetchingStatus] = useState(
    "Fetching Permaposts ..."
  );
  const axios = require("axios");

  const web3LoadStatus = useScript(
    "https://cdn.jsdelivr.net/npm/web3@latest/dist/web3.min.js"
  );

  useEffect(() => {
    if (typeof window != "undefined" && web3LoadStatus === "ready") {
      if (!xDaiWeb3) {
        setXDaiEthWeb3(new window.Web3("https://xdai-archive.blockscout.com/"));
      }
      if (!ethWeb3) {
        setEthWeb3(new window.Web3(process.env.ETH_RPC));
      }
    }
  }, [web3LoadStatus, xDaiWeb3, setXDaiEthWeb3, ethWeb3, setEthWeb3, router]);

  useEffect(() => {
    if (xDaiWeb3) {
      if (router?.query?.slug && router?.query?.slug.length > 0) {
        getBlocks(router?.query?.slug[0]);
      }
    }
  }, [xDaiWeb3, ethWeb3, router]);

  const getBlocks = async (address) => {
    setUserAddress(address);
    const postsBlocks = [];
    try {
      const result = await axios(
        `https://blockscout.com/xdai/mainnet/api?module=account&action=txlist&address=${address}`
      );
      await result.data.result.forEach(async (block) => {
        const hash = block?.hash;
        const tx_data_hex = block?.input;
        const tx_data_decoded = await abiDecoder.decodeMethod(tx_data_hex);
        if (!tx_data_decoded) {
          return;
        }
        const message_str = tx_data_decoded?.params[0].value.slice(12);
        // parse only the smoke signal message object
        try {
          const message_json = JSON?.parse(message_str);
          if (message_json.m.length >= 2) {
            const title = message_json.m[0];
            const body = message_json.m[2];
            const timestamp = block?.timeStamp;
            if (title !== "") {
              postsBlocks.push({
                blockHash: block.blockHash,
                hash,
                title,
                body,
                timestamp,
                blockchain: "xdai",
              });
            }
          }
        } catch (error) {}
      });
    } catch (error) {
      console.error("checkBlock", error);
    }
    try {
      const gettx = await axios(
        `https://api.etherscan.io/api?module=account&action=txlist&address=${address}&startblock=0&endblock=99999999&sort=asc&apikey=VWF8XCBQTDWZMVMXNN6JX81MK29VSDZEPQ`
      );
      await gettx.data.result.forEach(async (block) => {
        const tx_data_decoded = await abiDecoder.decodeMethod(block.input);
        if (
          tx_data_decoded !== undefined &&
          tx_data_decoded.params[0].value.indexOf("!smokesignal") >= 0 &&
          block.from === address
        ) {
          const message_str = tx_data_decoded?.params[0].value.slice(12);
          // parse only the smoke signal message object
          try {
            const message_json = JSON?.parse(message_str);
            if (message_json.m.length >= 2) {
              const title = message_json.m[0];
              const body = message_json.m[2];
              const hash = block.hash;
              const timestamp = block.timeStamp;
              if (title !== "") {
                postsBlocks.push({
                  blockHash: block.blockHash,
                  hash,
                  title,
                  body,
                  timestamp,
                  blockchain: "eth",
                });
              }
            }
          } catch (error) {}
        }
      });
    } catch (error) {
      console.error(error);
    }
    if (postsBlocks.length === 0) {
      setFetchingStatus("No SmokeSignal objects found.");
    } else {
      const sortedPostsBlocks = postsBlocks
        .sort(function (x, y) {
          return x.timestamp - y.timestamp;
        })
        .reverse();
      setPosts(sortedPostsBlocks);
    }
  };

  return (
    <Layout title={`Permapost | ${userAddress}`} description="Account view.">
      {userAddress !== "" && (
        <FlexDiv>
          <PhaceComponent>
            <PhaceElement messageFlag={userAddress} />
          </PhaceComponent>
          <StyledH1>{userAddress}</StyledH1>
        </FlexDiv>
      )}
      {posts.length === 0 && (
        <div>
          {" "}
          <strong>{fetchingStatus}</strong>
        </div>
      )}
      {posts &&
        posts.map((item, index) => {
          return (
            <PostAreaStyling key={index}>
              <DateStamp>
                {new Date(item.timestamp * 1000).toDateString()}
              </DateStamp>
              <Link
                href={`/view?blockchain=${item.blockchain}&tx=${item.hash}`}
                passHref
              >
                <StyledLink>
                  <h2>{item.title}</h2>
                </StyledLink>
              </Link>
              {item.body && item.body.length > 400 ? (
                <StyledMarkdown className="limit-height">
                  <ReactMarkdown remarkPlugins={[gfm]}>
                    {item.body}
                  </ReactMarkdown>
                </StyledMarkdown>
              ) : (
                <StyledMarkdown>
                  <ReactMarkdown remarkPlugins={[gfm]}>
                    {item.body}
                  </ReactMarkdown>
                </StyledMarkdown>
              )}
              {item.body && item.body.length > 400 && (
                <DateStamp>
                  <Link
                    href={`/view?blockchain=${item.blockchain}&tx=${item.hash}`}
                  >
                    <a>Read more</a>
                  </Link>
                </DateStamp>
              )}

              <AuthorPost xDaiWeb3={xDaiWeb3} ethWeb3={ethWeb3} item={item} />
            </PostAreaStyling>
          );
        })}
    </Layout>
  );
};

export default Author;

const StyledMarkdown = styled.div`
  &.limit-height {
    max-height: 129px;
    overflow-y: hidden;
    position: relative;
    &:after {
      content: "";
      height: 74px;
      width: 100%;
      background: none;
      background: linear-gradient(
        0deg,
        rgba(255, 255, 255, 1) 50%,
        rgba(255, 255, 255, 0) 100%
      );
      position: absolute;
      bottom: 0;
    }
  }
  ul,
  ol {
    list-style: inherit;
    margin-bottom: 1em;
    li {
      margin-left: 1.2em;
    }
  }
  i,
  em {
    font-style: italic;
  }
`;

const StyledH1 = styled.h1`
  word-break: break-word;
  margin: 0 0 0 0.5em;
`;

const PhaceComponent = styled.div`
  svg {
    border-radius: 5px;
  }
`;

const FlexDiv = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: left;
  position: sticky;
  top: 64px;
  @media screen and (max-width: 40em) {
    top: 61px;
  }
  background: rgb(255, 255, 255);
  background: linear-gradient(
    180deg,
    rgba(255, 255, 255, 1) 79%,
    rgba(255, 255, 255, 0) 100%
  );
  z-index: 1;
  padding-top: 1em;
  h1 {
    font-size: 1.8rem;
  }
  h2 {
    margin: 0 0 0 0.5em;
  }
  @media screen and (max-width: 40em) {
    h1 {
      font-size: 1.5rem;
    }
  }
  margin-bottom: 1rem;
`;

const StyledLink = styled.a`
  font-weight: inherit;
  text-decoration: none;
`;

const StyledInput = styled.input`
  width: 40%;
  top: 0;
  left: 0;
  padding: 0 0 1;
`;

const TipAreaStyling = styled.div`
  justify-content: center;
  align-items: center;
  text-align: left;
  padding: 0rem 0rem 0rem;
  margin: 1em 0;
  font-size: 100%;
  width: 100%;
`;

const keyFrameFadeIn = keyframes`
0% {
  top: 2px;
  visibility: visible;
}
100% {
  opacity: 1;
  visibility: visible;
}
`;

const DateStamp = styled.div`
  font-size: 0.6666666667em;
  color: rgba(117, 117, 117, 1);
  a {
    font-weight: normal;
    margin-top: 0;
    display: block;
  }
`;

const PostAreaStyling = styled.div`
  h2 {
    margin: 0.3em 0 0.5em 0;
  }
  visibility: hidden;
  justify-content: center;
  align-items: center;
  text-align: left;
  font-size: 100%;
  margin: 0;
  width: 100%;
  border-bottom: solid 0.0625em rgba(230, 230, 230, 1);
  padding: 1em 0;
  &:last-child {
    border-bottom: none;
  }
  position: relative;
  animation-name: ${keyFrameFadeIn};
  animation-duration: 0.5s;
  animation-iteration-count: 1;
  animation-fill-mode: forwards;
  &:nth-child(5n + 1) {
    animation-delay: 0.2s;
  }
  &:nth-child(5n + 2) {
    animation-delay: 0.3s;
  }
  &:nth-child(5n + 3) {
    animation-delay: 0.4s;
  }
  &:nth-child(5n + 4) {
    animation-delay: 0.5s;
  }
  &:nth-child(5n + 5) {
    animation-delay: 0.6s;
  }
`;
