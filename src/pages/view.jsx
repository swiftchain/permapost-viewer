import { useState, useEffect } from "react";

import { useRouter } from "next/router";

import styled from "styled-components";

import Layout from "../components/Layout";
import Post from "../components/Post";

import markdownToTxt from "markdown-to-txt";
import { decode } from "html-entities";

import Web3 from "web3";

const BLOCKCHAINS = process.env.BLOCKCHAINS;
const DEFAULT_BLOCKCHAIN = Object.entries(BLOCKCHAINS)[1][0];

const abiDecoder = require("abi-decoder");
const ABI = require("../lib/abi_2021_02_25.json");
abiDecoder.addABI(ABI);

export default function View({ permapostBlock }) {
  const router = useRouter();

  // used to make the tx hash input field controlled
  const [txHashInput, setTxHashInput] = useState("");
  // used to make the tx blockchain dropdown controlled
  const [txBlockchain, setTxBlockchain] = useState(DEFAULT_BLOCKCHAIN);
  // details of post to pull from blockchain
  const [txLookupDetails, setTxLookupDetails] = useState(null);

  useEffect(() => {
    // If the URL contains a tx query parameter
    if (router?.query?.tx) {
      // if the URL query parameters differ from the saved state
      if (
        router?.query?.tx !== txLookupDetails?.hash ||
        router?.query?.blockchain !== txLookupDetails?.blockchain_id
      ) {
        // get the target blockchain
        const blockchain_id =
          router?.query?.blockchain?.toLowerCase?.() || DEFAULT_BLOCKCHAIN;
        // set the tx lookup details. This causes the new post to be fetched
        setTxLookupDetails({
          hash: router?.query?.tx,
          blockchain_id,
          blockchain_name: BLOCKCHAINS[blockchain_id],
        });
        // Change the content of the tx hash input field
        setTxHashInput(router?.query?.tx || "");
        if (router?.query?.blockchain) {
          // update the blockchain if the blockchain
          // was specified with in a query parameter
          setTxBlockchain(router?.query?.blockchain);
        }
      }
    }
  }, [router, txLookupDetails, setTxLookupDetails, txHashInput]);

  // Change the url, which will cause the page to reload and
  // a new post to be fetched
  const isValidHex = (input) => {
    const legend = "0123456789abcdef";
    if (input.length !== 66) {
      return false;
    }

    if (input.slice(0, 2) !== "0x") {
      return false;
    }

    for (var i = 2; i < input.length; i++) {
      if (legend.includes(input[i].toLowerCase())) {
        continue;
      }
      return false;
    }
    return true;
  };

  const viewPost = () => {
    if (isValidHex(txHashInput)) {
      if (
        txHashInput !== txLookupDetails?.hash ||
        txBlockchain !== txLookupDetails?.blockchain_id
      ) {
        // only fetch the post if the requested data is new
        router.push({
          pathname: router.pathname,
          query: { blockchain: txBlockchain, tx: txHashInput },
        });
      }
    } else {
      alert("Invalid transaction entered.");
    }
  };

  return (
    <Layout title={permapostBlock.title} description={permapostBlock.body}>
      <ViewAreaStyling>
        <StyledMaxWidth>
          <InputStyling>
            <StyledLabel htmlFor="chain">Blockchain</StyledLabel>
            <StyledSelect
              value={txBlockchain}
              onChange={(event) => setTxBlockchain(event.target.value)}
              name="chain"
            >
              {Object.entries(BLOCKCHAINS).map(([chain_id, chain_name]) => (
                <option key={chain_id} value={chain_id}>
                  {chain_name}
                </option>
              ))}
            </StyledSelect>
          </InputStyling>
          <InputStyling>
            <StyledLabel htmlFor="tx">Transaction Hash</StyledLabel>
            <StyledInput
              name="tx"
              value={txHashInput}
              onChange={(event) => setTxHashInput(event.target.value)}
            />
          </InputStyling>
          <div>
            <StyledButton onClick={viewPost}>View</StyledButton>
          </div>
        </StyledMaxWidth>
        <Post tx={txLookupDetails} />
      </ViewAreaStyling>
    </Layout>
  );
}

const StyledMaxWidth = styled.div`
  max-width: 46.4285em;
  margin: auto;
  display: flex;
  padding-top: 2em;
  & > div {
    position: relative;
    flex: 0;
  }
  & > div:nth-child(2) {
    flex: 1;
    margin: 0 0.75em;
  }
  @media screen and (max-width: 40em) {
    display: block;
    padding-top: 1em;
    & > div {
      flex: 0;
    }
    & > div:nth-child(2) {
      flex: 0;
      margin: 0;
    }
  }
`;

const StyledH1 = styled.h1`
  text-align: center;
`;
const StyledLabel = styled.label``;
const StyledSelect = styled.select``;
const StyledInput = styled.input``;
const InputStyling = styled.div`
  margin: 0 0 1em;
  flex: 1;
`;
const StyledButton = styled.button`
  margin: 1rem 0;
  flex: 1;
  height: 56px;
  margin-top: 1.9em;
`;
const InfoStyling = styled.div`
  padding: 0.5rem 0;
  text-align: center;
  word-break: break-word;
`;
const ViewAreaStyling = styled.div`
  display: block;
  width: 100%;
`;

export async function getServerSideProps({ query }) {
  const tx = query.tx;
  const blockChain = query.blockchain;

  let web3RPC;

  if (blockChain === "xdai") {
    web3RPC = "https://xdai-archive.blockscout.com/";
  } else {
    web3RPC = process.env.ETH_RPC;
  }

  const web3 = new Web3(new Web3.providers.HttpProvider(web3RPC));

  let permapostBlock = { title: "View permapost", body: "Permapost" };

  // Use web3 getTransaction to get the json transaction object, then we log only the values we want
  try {
    const getThis = await web3.eth.getTransaction(tx, function (err, block) {
      if (!err) {
        // Crash if tx not found problem from Here
        const tx_data_hex = block?.input;
        if (!tx_data_hex) {
          return;
        }
        const tx_data_decoded = abiDecoder.decodeMethod(tx_data_hex);
        if (!tx_data_decoded) {
          return;
        }
        // to here
        const message_str = tx_data_decoded?.params[0].value.slice(12);
        let info;
        const message_json = JSON?.parse(message_str);
        if (message_json.m.length >= 2) {
          const title = message_json.m[0];
          const body = message_json.m[2];
          if (title !== "") {
            info = {
              title,
              body: decode(markdownToTxt(body).substring(0, 163), {
                level: "html5",
              }),
              blockchain: blockChain,
            };
          }
        }
        permapostBlock = info;
        console.log("permapostBlock", info);
      } else {
        console.log("Error!", err); // Dump errors here
      }
    });
  } catch (error) {
    console.error(error);
    return;
  }
  //Pass data to the page via props
  return { props: { permapostBlock } };
}
