import { createGlobalStyle, ThemeProvider } from "styled-components";
import ResetStyles from "./../styles/reset";
import GlobalStyles from "./../styles/globalStyles";

import "./../styles/page-loader.css";

const GlobalStyle = createGlobalStyle`
  ${ResetStyles}
  ${GlobalStyles}
`;

const theme = {
  colors: {
    primary: "#fafafa",
  },
};

function MyApp({ Component, pageProps }) {
  return (
    <>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
      </ThemeProvider>
    </>
  );
}

export default MyApp;
