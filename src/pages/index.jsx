import { useState, useEffect } from "react";
import detectEthereumProvider from "@metamask/detect-provider";

import Cookies from "js-cookie";
import Link from "next/link";
import Image from "next/image";
import styled from "styled-components";
import ReactMarkdown from "react-markdown";
import gfm from "remark-gfm";
import Layout from "../components/Layout";
import useScript from "../lib/useScript";
import CONTRACT_ABI from "../lib/abi_2021_02_25.json";
import PermapostTnC_ABI from "../lib/PermapostTnC.json";
import Modal from "../components/Modal";
import FirstTimeMessages from "../components/FirstTimeMsgs";

import aspectRatioSVG from "./../styles/aspect-ratio.svg";

const DEFAULT_BLOCKCHAIN = Object.entries(process.env.BLOCKCHAINS)[1][0];
const DEFAULT_SUPPORTED_BLOCKCHAINS = Object.entries(process.env.BLOCKCHAINS);
const DEFAULT_BLOCKCHAIN_NAME = Object.entries(process.env.BLOCKCHAINS)[1][1];

export default function Post() {
  const [web3, setWeb3] = useState(null);
  const [contract, setContract] = useState(null);
  const [tnCcontract, setTnCContract] = useState(null);
  const [walletAddress, setWalletAddress] = useState(null);
  const [ethPerUsd, setEthPerUsd] = useState(0);

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [postText, setPostText] = useState("");
  const [burnAmount, setBurnAmount] = useState(0);
  const [gasProps, setGasProps] = useState({
    gasPrice: 1000000000,
  });
  const [ethGasProps, setEthGasProps] = useState(null);
  const [burnAmountUsd, setBurnAmountUsd] = useState(0);
  const [donateAmount, setDonateAmount] = useState(0);
  const [createdPostTxHash, setCreatedPostTxHash] = useState("");
  const [createdTnCTxHash, setCreatedTnCTxHash] = useState("");
  const [selectedBlockchain, setSelectedBlockchain] = useState("");
  const [createdPostBlockChain] = useState(DEFAULT_BLOCKCHAIN);
  const [createdPostSetBlockChains] = useState(DEFAULT_SUPPORTED_BLOCKCHAINS);
  const [showFirstTimeMessages, setShowFirstTimeMessages] = useState(false);
  const [metamaskErrorModalVisible, setMetamaskErrorModalVisible] =
    useState(false);
  const [suggestxDaiModalVisible, setSuggestxDaiModalVisible] = useState(false);
  const [unsupportedChainModalVisible, setUnsupportedChainModalVisible] =
    useState(false);

  const [textAreaRows, setTextAreaRows] = useState(0);

  const [postStatus, setPostStatus] = useState("");
  const [posting, setPosting] = useState(false);
  const [signStatus, setSignStatus] = useState("");
  const [signing, setSigning] = useState(false);

  const [faucetOptionVisible, setFaucetOptionVisible] = useState(false);
  const [faucetButtonDisabled, setFaucetButtonDisabled] = useState(false);
  const [faucetButtonText, setFaucetButtonText] = useState(
    "Request Free xDai from Platform"
  );
  const [addedxdaiSuccess, setAddedxdaiSuccess] = useState(false);

  const [metaMaskChanged, setMetaMaskChanged] = useState("");

  const [showBraveModal, setShowBraveModal] = useState(false);

  const [connectWallet, setConnectWallet] = useState("");

  const [showOptionsDropdown, setShowOptionsDropdown] = useState(false);

  const [editFullScreen, setEditFullScreen] = useState(false);

  // we need to load the web3 script like this due to server side rendering during build
  const web3LoadStatus = useScript(
    "https://cdn.jsdelivr.net/npm/web3@latest/dist/web3.min.js"
  );

  let gas = { gasPrice: 1000000000 };

  let supportedBlockchains = null;

  useEffect(() => {
    // only init the web3 object once, and only if the script has been loaded
    if (
      typeof window != "undefined" &&
      window.ethereum !== undefined &&
      web3LoadStatus === "ready" &&
      !web3
    ) {
      (async () => {
        console.log("Startup, test eth_requestAccounts");
        let testPassed = false;

        try {
          setMetamaskErrorModalVisible(false);
          const sendTest = await window.ethereum.send("eth_requestAccounts");
          console.log("sendTest", sendTest);
          testPassed = true;
        } catch (error) {
          setMetamaskErrorModalVisible(true);
          console.log("sendTest Error: ", error);
        }

        if (testPassed) {
          console.log("updating web3");
          const newWeb3 = new window.Web3(window.ethereum);
          console.log("newWeb3", newWeb3);
          const accounts = await newWeb3.eth.getAccounts();
          console.log("accounts", accounts);
          setWalletAddress(accounts[0]);
          setWeb3(newWeb3);
        }
      })();
    }
  }, [connectWallet, web3, walletAddress]);

  useEffect(() => {
    // only initialise the contract once, and only if web3 has been initialised.
    let new_contract = null;
    let new_TnC_contract = null;
    if (web3 && walletAddress) {
      web3.eth.getChainId().then((chainID) => {
        // Detect which blockchain MM is connected to. ID 1 means Ethereum, ID 100 means xDai.
        if (chainID == 1) {
          console.log("Ethereum network detected in MM.");
          setSelectedBlockchain("ethereum");
          // If on Ethereum, suggest xDai to lower fees.
          setSuggestxDaiModalVisible(true);
          new_contract = new web3.eth.Contract(
            CONTRACT_ABI,
            process.env.ETH_CONTRACT_ADDRESS
          );
          new_TnC_contract = new web3.eth.Contract(
            PermapostTnC_ABI,
            process.env.ETH_TnC_CONTRACT_ADDRESS
          );
        } else if (chainID == 100) {
          console.log("xDai network detected in MM.");
          web3.eth.getBalance(walletAddress, (err, balance) => {
            balance = parseFloat(web3.utils.fromWei(balance, "ether"));
            console.log("User balance: ", balance);
            if (balance === 0.0) {
              setFaucetOptionVisible(true);
            }
          });
          setSelectedBlockchain("xdai");
          setSuggestxDaiModalVisible(false);
          new_contract = new web3.eth.Contract(
            CONTRACT_ABI,
            process.env.XDAI_CONTRACT_ADDRESS
          );
          new_TnC_contract = new web3.eth.Contract(
            PermapostTnC_ABI,
            process.env.XDAI_TnC_CONTRACT_ADDRESS
          );
        } else {
          // Instruct user to select a supported blockchain.
          setSelectedBlockchain("Unsupported Network");
          setUnsupportedChainModalVisible(true);
          supportedBlockchains = createdPostSetBlockChains;
          return;
        }
        setContract(new_contract);
        setTnCContract(new_TnC_contract);
        (async () => {
          console.log("GasProps ======", gasProps);
          const ethGasEstimate = await new_contract.methods
            .EthPrice()
            .estimateGas();
          const ethPrice = await new_contract.methods.EthPrice().call({
            from: walletAddress,
          });
          console.log("ethPrice", ethPrice);
          let retrievedGasPrice = 0;
          const getretrievedGasPrice = await web3.eth.getGasPrice(function (
            e,
            r
          ) {
            retrievedGasPrice = r;
          });

          const ethToUsd = await new_contract.methods
            .ethToUsd(1)
            .call({ from: walletAddress });
          console.log("ethToUsd", ethToUsd);
          const new_ethPerUsd = ethToUsd / 10 ** 18;
          console.log("new_ethPerUsd", new_ethPerUsd);
          setEthPerUsd(new_ethPerUsd);
          if (selectedBlockchain == "xdai") {
            setGasProps({ gasPrice: 1000000000 });
          } else if (selectedBlockchain == "ethereum") {
            setGasProps("");
          }
        })();

        console.log("new_contract", new_contract);
        console.log("new_contract.methods", new_contract.methods);
        new_contract.defaultAccount = walletAddress;
      });
    }
  }, [web3, walletAddress, metaMaskChanged]);

  useEffect(() => {
    checkIfBrave();
    if (window.ethereum) {
      // Metamask account change
      window.ethereum.on("accountsChanged", function (accounts) {
        if (accounts.length > 0) {
          setWalletAddress(accounts[0]);
          setMetamaskErrorModalVisible(false);
        } else {
          setWalletAddress(null);
          setMetamaskErrorModalVisible(true);
        }
      });
      // Network account change
      window.ethereum.on("chainChanged", function (networkId) {
        console.log(networkId);
        if (networkId === "0x1") {
          setSelectedBlockchain("ethereum");
          setUnsupportedChainModalVisible(false);
          setMetaMaskChanged("0x1");
        } else if (networkId === "0x64") {
          setSelectedBlockchain("xdai");
          setMetaMaskChanged("0x64");
          setUnsupportedChainModalVisible(false);
        } else {
          setUnsupportedChainModalVisible(true);
        }
      });
    } else {
      console.warn("No web3 detected.");
    }
  });

  const checkIfBrave = () => {
    if (window.navigator.brave && !Cookies.get("isItABraveBrowser")) {
      setShowBraveModal(true);
    }
  };

  const checkForSignature = async () => {
    try {
      let signed = await tnCcontract.methods.acceptedTnCs(walletAddress).call();
      console.log("Signed Return: ", signed);
      return signed;
    } catch (error) {
      console.log("Error checking for signature: ");
      console.error(error);
      return false;
    }
  };

  // This is executed before a post is made to ensure user has accepted the risks.
  const risksAccepted = async () => {
    if ((await checkForSignature()) === false) {
      setShowFirstTimeMessages(true);
      console.log("Show 1st time messages");
      //playWithTestCounter();
    } else {
      setSigning(false);
      console.log("Make Post");
      createPost();
    }
  };

  // make a smokesignal post to the blockchain
  const createPost = async () => {
    setPosting(true);
    setPostStatus("Post Mining...");
    const smokesignal = {
      m: [title, description, postText],
      v: 3,
      c: { topic: "Permapost" },
    };
    console.log("smokesignal", smokesignal);

    // Run the burnMessage function from the contract
    // only include burn amount and donate amount if the burnAmount is set
    try {
      const postReceipt = burnAmount
        ? await contract.methods
            .burnMessage(
              `!smokesignal${JSON.stringify(smokesignal)}`,
              donateAmount
            )
            .send({
              from: walletAddress,
              value: burnAmount,
              gasPrice: gasProps.gasPrice,
            })
        : await contract.methods
            .burnMessage(`!smokesignal${JSON.stringify(smokesignal)}`, 0)
            .send({
              from: walletAddress,
              gasPrice: gasProps.gasPrice,
            });

      console.log("postReceipt", postReceipt);

      // retrieve the new post's tx hash from the post receipt to allow the
      // user to easily view their new post
      const txHash = postReceipt?.transactionHash;
      console.log("txHash", txHash);
      setCreatedPostTxHash(txHash);
      setPosting(true);
      setPostStatus("Permapost Ready");
      console.log(273, postStatus);
    } catch (error) {
      setPosting(true);
      setPostStatus("Failed to post.");
    }
  };

  const acceptTermsAndConditions = async () => {
    setShowFirstTimeMessages(false);
    console.log("Accepting TnCs");
    setSigning(true);
    setSignStatus("T&C Signature Mining...");

    try {
      let postReceipt = await tnCcontract.methods
        .acceptTermsAndConditions()
        .send({
          from: walletAddress,
          value: 0,
          gasPrice: gasProps.gasPrice,
        });

      console.log("postReceipt", postReceipt);

      // retrieve the new post's tx hash from the post receipt to allow the
      // user to easily view their new post
      const txHash = postReceipt?.transactionHash;
      console.log("txHash", txHash);
      setCreatedTnCTxHash(txHash);
      setSigning(true);
      setSignStatus("Accepted Terms and Conditions");
    } catch (error) {
      setSigning(true);
      setSignStatus("Failed to sign.");
      console.error(error);
    }
  };

  const resetForm = () => {
    setTitle("");
    setDescription("");
    setPostText("");
    setCreatedPostTxHash("");
    setCreatedTnCTxHash("");
    setTextAreaRows(0);
    setPostStatus("");
    setSignStatus("");
  };

  // handle burn amount input field changes
  const handleBurnAmountChange = (event) => {
    const new_burn = event.target.value;
    if (new_burn < donateAmount) {
      setDonateAmount(new_burn);
    }
    setBurnAmount(new_burn);
  };

  const checkRequestedBalance = async () => {
    let balance = 0;
    await web3.eth.getBalance(walletAddress, (err, balance) => {
      balance = parseFloat(web3.utils.fromWei(balance, "ether"));
    });
    const refreshBalance = setInterval(async () => {
      await web3.eth.getBalance(walletAddress, (err, accountBalance) => {
        let floatBalance = parseFloat(
          web3.utils.fromWei(accountBalance, "ether")
        );
        if (floatBalance !== balance) {
          setAddedxdaiSuccess(true);
          setFaucetButtonText(`xDai is in your wallet`);
          clearInterval(refreshBalance);
          setFaucetButtonDisabled(false);
        }
      });
    }, 1000);
  };

  const faucetxDaiRequest = async () => {
    setFaucetButtonDisabled(true);
    setAddedxdaiSuccess(false);
    setFaucetButtonText("Requesting xDai ...");

    const response = await fetch(
      "https://personal-rxyx.outsystemscloud.com/ERC20FaucetRest/rest/v1/send?In_ReceiverErc20Address=" +
        walletAddress +
        "&In_Token=fNjthLRDiJCyyLZTmZQvQVAXsTBKjH44CiMMETjPCjuyqMGhGMxY6PvAmAjQqsbx"
    )
      .then(async (response) => {
        const data = await response.json();

        //Interpret http response
        console.log("Faucet Response:", data);

        if (!response.ok) {
          // get error message from body or default to response statusText
          const error = (data && data.message) || response.statusText;
          return Promise.reject(error);
        }

        if (data.message === "Success") {
          setFaucetButtonText(`Requested! Balance is updating ...`);
          checkRequestedBalance();
        } else if (data.status === false) {
          if (
            data.message === "You can only request tokens once in 24 hour(s)"
          ) {
            setFaucetButtonText("24h Request Limit Reached");
          } else {
            setFaucetButtonText("Request Failed. Click to retry");
            console.log("Could not make faucet request: ", data.message);
            setFaucetButtonDisabled(false);
          }
        }
      })
      .catch((error) => {
        console.error("Error in faucet request: ", error);
        setFaucetButtonText("Request Failed. Click to retry");
        setFaucetButtonDisabled(false);
      });
  };

  const addNetwork = () => {
    const params = [
      {
        chainId: "0x64",
        chainName: "xDAI Chain",
        nativeCurrency: {
          name: "xDAI Chain",
          symbol: "xDAI",
          decimals: 18,
        },
        rpcUrls: ["https://rpc.xdaichain.com"],
        blockExplorerUrls: ["https://blockscout.com/xdai/mainnet"],
      },
    ];

    window.ethereum
      .request({ method: "wallet_addEthereumChain", params })
      .then(() => console.log("Success"))
      .catch((error) => console.log("Error", error));
  };

  const changeToXDAINetwork = () => {
    const params = [
      {
        chainId: "0x64",
      },
    ];
    if (window.ethereum === undefined) {
      setMetamaskErrorModalVisible(true);
      return false;
    }
    window.ethereum
      .request({ method: "wallet_addEthereumChain", params })
      .then(() => console.log("Success"))
      .catch((error) => {
        console.log("Error", error);
        addNetwork();
      });
    setConnectWallet(Date.now());
  };

  const changeToEthNetwork = async () => {
    const params = [
      {
        chainId: "0x1",
      },
    ];

    if (window.ethereum === undefined) {
      setMetamaskErrorModalVisible(true);
      return false;
    }

    window.ethereum
      .request({ method: "wallet_switchEthereumChain", params })
      .then(() => console.log("========= ETH = Success"))
      .catch((error) => console.log("===== ETH = Error", error));
    setConnectWallet(Date.now());
  };

  useEffect(() => {
    if (editFullScreen) {
      document.querySelector("body").classList.add("true");
    } else {
      document.querySelector("body").classList.remove("true");
    }
  }, [editFullScreen]);

  return (
    <Layout title="Permapost" description="Create a permanent post.">
      <Modal
        show={showBraveModal}
        hideModal={() => {
          setShowBraveModal(false);
          Cookies.set("isItABraveBrowser", "showedTrue", {
            expires: 365,
          });
        }}
      >
        <StyledInfoModalh2>Using the Brave browser?</StyledInfoModalh2>
        <ErrorMessageAreaStyling>
          <p>
            Permapost supports using <strong>MetaMask</strong> and not the built
            in <strong>Brave wallet</strong>.
          </p>
          <p>
            Make sure your Brave Ethereum provider dropdown is set to{" "}
            <strong>{`"ASK"`}</strong>.
          </p>
          <StyledOrderedList>
            <li>
              Go to Extensions Settings:{" "}
              <strong>brave://settings/extensions</strong>
            </li>
            <li>
              Wallet <strong>&gt;</strong> Ethereum provider for using Dapps{" "}
              <strong>&gt;</strong> <strong>{`"ASK"`}</strong>
            </li>
          </StyledOrderedList>
        </ErrorMessageAreaStyling>
        <MessageButtonStyling>
          <p>
            <button
              className="blue-btn"
              onClick={() => {
                setShowBraveModal(false);
                Cookies.set("isItABraveBrowser", "showedTrue", {
                  expires: 365,
                });
              }}
            >{`All done? Continue.`}</button>
          </p>
        </MessageButtonStyling>
      </Modal>
      <Modal show={posting} hideModal={() => setPosting(false)}>
        {!createdPostTxHash ? (
          <StyledModalH2>{postStatus}</StyledModalH2>
        ) : (
          <div>
            <StyledModalH2>Permapost Ready</StyledModalH2>
            If you would like to view your new permapost, please{" "}
            <a
              target="_blank"
              rel="noreferrer"
              href={`/view?blockchain=${selectedBlockchain}&tx=${createdPostTxHash}`}
            >
              click this link
            </a>
            .
          </div>
        )}
      </Modal>
      <Modal show={signing} hideModal={() => setSigning(false)}>
        {!createdTnCTxHash ? (
          <StyledModalH2>{signStatus}</StyledModalH2>
        ) : (
          <div>
            <MessageTitleAreaStyling>
              <StyledModalH2>Terms and Conditions Accepted</StyledModalH2>
            </MessageTitleAreaStyling>

            <MessageAreaStyling>
              We are happy to have you! You can now continue posting.
            </MessageAreaStyling>
            <StyledButton
              className={
                title.length > 0 &&
                postText.length > 0 &&
                postText.length <= 500000 &&
                web3
                  ? "clickable"
                  : "clickable-inactive"
              }
              onClick={risksAccepted}
            >
              Post First Permapost
            </StyledButton>
          </div>
        )}
      </Modal>
      <Modal
        show={metamaskErrorModalVisible}
        hideModal={() => setMetamaskErrorModalVisible(false)}
      >
        <MessageTitleAreaStyling>
          <StyledInfoModalh2>
            Permapost requires the latest version of MetaMask
          </StyledInfoModalh2>
        </MessageTitleAreaStyling>
        <div>
          <ErrorMessageAreaStyling>
            <p>{`You'll need to setup the Metamask extension to publish a permapost. Metamask can be installed for the following browsers:`}</p>
            <ul>
              <li>
                <a
                  href="https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en"
                  rel="noreferrer"
                  target="_blank"
                >
                  Brave and Chrome
                </a>{" "}
                Web store
              </li>
              <li>
                <a
                  href="https://addons.mozilla.org/en-US/firefox/addon/ether-metamask/"
                  rel="noreferrer"
                  target="_blank"
                >
                  Firefox
                </a>{" "}
                Add-ons
              </li>
            </ul>
          </ErrorMessageAreaStyling>
        </div>
        <MessageButtonStyling>
          <p>
            <button
              className="blue-btn"
              onClick={() => {
                window.location.reload();
              }}
            >
              All done? Click to reload.
            </button>
          </p>
        </MessageButtonStyling>
      </Modal>

      <Modal
        show={suggestxDaiModalVisible}
        hideModal={() => setSuggestxDaiModalVisible(false)}
      >
        <StyledInfoModalh2>Blockchain Suggestion</StyledInfoModalh2>
        <div>
          <ErrorMessageAreaStyling>
            <p>
              {`In order to pay much lower fees, we suggest using the xDai network instead of Ethereum.`}
            </p>
            <p>
              <StyledCenterButton
                className="blue-btn"
                onClick={changeToXDAINetwork}
              >
                Add/Switch to xDai
              </StyledCenterButton>
            </p>
          </ErrorMessageAreaStyling>
        </div>
      </Modal>

      <Modal show={unsupportedChainModalVisible}>
        <MessageTitleAreaStyling>
          <h2>Unsupported Blockchain Selected</h2>
        </MessageTitleAreaStyling>
        <div>
          <ErrorMessageAreaStyling>
            {`Please select a supported blockchain network in MetaMask.`}{" "}
            <br></br>
            <br></br>
            {`Currently supported networks:`}
            {createdPostSetBlockChains.map((chain, i) => (
              <li key={i}>{chain[1]}</li>
            ))}
          </ErrorMessageAreaStyling>
          <MessageButtonStyling>
            <button
              onClick={() => {
                window.location.reload();
              }}
            >
              Reload
            </button>
          </MessageButtonStyling>
        </div>
      </Modal>

      <FirstTimeMessages
        showMessages={showFirstTimeMessages}
        setShowMessages={setShowFirstTimeMessages}
        welcomeWalletAddress={walletAddress}
        acceptTnCs={acceptTermsAndConditions}
      />

      <StyledH1>Create a Permapost</StyledH1>

      <StyledStuck>
        Stuck? Freedom of speech is our priority.{" "}
        <a href="mailto:support@foundrydao.com?subject=Permapost">
          {`Reach out and we'll help ASAP!`}
        </a>
      </StyledStuck>

      <PostAreaStyling>
        <InputStyling>
          <StyledLabel htmlFor="title">Title</StyledLabel>
          <StyledInput
            name="title"
            value={title}
            placeholder="Title"
            onChange={(event) => setTitle(event.target.value)}
          />
        </InputStyling>
        <Row xsNoflex className={editFullScreen ? "full-screen" : ""}>
          <Col
            className={editFullScreen ? "full-screen dark-theme" : ""}
            size={1}
          >
            <InputStyling>
              <EditStyledLabel htmlFor="content">
                Content
                <CharCount className={postText.length > 500000 ? "orange" : ""}>
                  {postText.length
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}{" "}
                  of 500,000
                </CharCount>
              </EditStyledLabel>
            </InputStyling>
            <InputStyling>
              <StyledTextarea
                style={{ height: textAreaRows + "px" }}
                rows={40}
                cols={150}
                value={postText}
                onChange={(event) => {
                  setTextAreaRows(
                    event.target.value.length === 0
                      ? 0
                      : event.target.scrollHeight
                  );
                  setPostText(event.target.value);
                }}
                name="content"
                placeholder="Heading&#10;=======&#10;Paragraphs are separated by a blank line."
              />
            </InputStyling>
          </Col>
          <Col className={editFullScreen ? "full-screen" : ""} size={1}>
            <PostPreviewLabelStyling>
              Post preview
              <span
                onClick={() => {
                  setEditFullScreen(!editFullScreen);
                }}
              >
                Fullscreen
                <ToggleButton className={editFullScreen ? "active" : ""}>
                  <span></span>
                </ToggleButton>
              </span>
            </PostPreviewLabelStyling>
            <PostPreviewStyling className="markdownPreview">
              <ReactMarkdown remarkPlugins={[gfm]}>{postText}</ReactMarkdown>
            </PostPreviewStyling>
          </Col>
        </Row>
        <InputStyling>
          <StyledLabel htmlFor="description">Description for SEO</StyledLabel>
          <StyledInput
            name="description"
            value={description}
            placeholder="SEO Description (Optional)"
            onChange={(event) => setDescription(event.target.value)}
          />
        </InputStyling>
        <InputStyling>
          <StyledLabel htmlFor="chain">Blockchain</StyledLabel>
          <StyledSelectedNetworkDropdown
            className={showOptionsDropdown}
            onClick={() => setShowOptionsDropdown(!showOptionsDropdown)}
          >
            {selectedBlockchain === "" ? (
              "Choose network"
            ) : (
              <div>
                <span className="connected-circle"></span>
                Connected to {selectedBlockchain}
              </div>
            )}
          </StyledSelectedNetworkDropdown>
          <StyledOptionsNetworkDropdown className={showOptionsDropdown}>
            <button
              onClick={() => {
                changeToXDAINetwork();
                setShowOptionsDropdown(false);
              }}
            >
              xDai
            </button>
            <button
              onClick={() => {
                changeToEthNetwork();
                setShowOptionsDropdown(false);
              }}
            >
              Ethereum
            </button>
          </StyledOptionsNetworkDropdown>
        </InputStyling>
        <MessageButtonStyling>
          {faucetOptionVisible === true && (
            <div>
              <p className="text-center">
                <strong>{`Note: Your xDai wallet is currently empty.`}</strong>
              </p>
              <StyledButton
                onClick={faucetxDaiRequest}
                disabled={faucetButtonDisabled}
                className={addedxdaiSuccess ? "added-xdai" : ""}
              >
                {faucetButtonText}
              </StyledButton>
            </div>
          )}
        </MessageButtonStyling>
        {!createdPostTxHash ? (
          <div>
            <StyledButton
              className={
                title.length > 0 &&
                postText.length > 0 &&
                postText.length <= 500000 &&
                web3
                  ? "clickable"
                  : "clickable-inactive"
              }
              onClick={risksAccepted}
            >
              Post
            </StyledButton>
            {postText.length > 500000 && (
              <LimitedChar>
                <div className="arrow-up"></div>
                Permaposts are limited to 500,000 characters. Your draft
                currently has{" "}
                {postText.length
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                .
              </LimitedChar>
            )}
          </div>
        ) : (
          <div>
            <StyledButton onClick={resetForm}>Reset the form</StyledButton>
            <InfoStyling>
              If you would like to view your new permapost, please{" "}
              <Link
                href={{
                  pathname: "/view/",
                  query: {
                    blockchain: selectedBlockchain,
                    tx: createdPostTxHash,
                  },
                }}
              >
                <a>click this link</a>
              </Link>
            </InfoStyling>
          </div>
        )}
      </PostAreaStyling>
    </Layout>
  );
}

const ToggleButton = styled.div`
  display: inline-block;
  border-radius: 10px;
  width: 40px;
  height: 20px;
  background: #ffffff;
  position: relative;
  top: 3px;
  margin-left: 10px;
  span {
    position: absolute;
    left: 0;
    width: 20px;
    height: 20px;
    background: #fff;
    transition: all 0.25s ease;
    border-radius: 10px;
    background: #999;
  }
  &:hover {
    span {
      left: 4px;
    }
  }
  &.active {
    span {
      margin-left: -20px;
      left: 100%;
      background: rgb(46 41 66);
      transition: all 0.25s ease;
    }
    &:hover {
      span {
        left: calc(100% - 4px);
      }
    }
  }
`;

const Row = styled.div`
  display: flex;
  margin-bottom: 1rem;
  border: 1px solid rgba(34, 36, 38, 0.15);
  border-radius: 5px;
  &.full-screen {
    position: fixed;
    top: 0;
    left: 0;
    z-index: 10;
    background: #fff;
    height: 100vh;
    border: none;
    border-radius: 0;
    width: 100%;
  }
  ${(props) =>
    props.xsNoflex
      ? `@media screen and (max-width: 40em) {
        display: block;
      }`
      : ``}
`;

const Col = styled.div`
  flex: 1;
  &:first-child {
    border-right: 1px solid #ddd;
  }
  > div:first-child {
    position: sticky;
    top: 63px;
    z-index: 1;
    @media screen and (max-width: 40em) {
      top: 60px;
    }
  }
  &.full-screen {
    overflow: auto;
    > div:first-child {
      position: sticky;
      top: 0;
      z-index: 1;
    }
    &.dark-theme {
      background: #000000;
      color: #ffffff;
      div,
      textarea {
        background: #000000;
        color: #ffffff;
      }
    }
  }
`;

const StyledModalH2 = styled.h2`
  margin-top: 0.5em;
`;

const CharCount = styled.span`
  float: right;
  font-weight: normal;
  font-size: 0.8em;
  &.orange {
    color: #f69b1e;
  }
`;

const LimitedChar = styled.div`
  text-align: center;
  background: #f69b1e;
  border-radius: 3px;
  display: block;
  margin: auto;
  max-width: max-content;
  padding: 0.5em 1em;
  position: relative;
  .arrow-up {
    width: 0;
    height: 0;
    position: absolute;
    top: -9px;
    left: 50%;
    margin-left: -5px;
    border-left: 10px solid transparent;
    border-right: 10px solid transparent;

    border-bottom: 10px solid #f69b1e;
  }
`;

const StyledStuck = styled.p`
  text-align: center;
  font-size: 0.7em;
  a {
    display: block;
  }
`;

const StyledSelectedNetworkDropdown = styled.div`
  padding: 0.5em 4em 0.5em 1em;
  border-radius: 0.125em;
  background: #fff;
  border: 0.071em solid rgba(34, 36, 38, 0.15);
  cursor: pointer;
  display: inline-block;
  position: relative;
  .connected-circle {
    background: #25dc6a;
    border-radius: 100%;
    width: 10px;
    height: 10px;
    display: inline-block;
    margin-right: 0.7em;
  }
  &:after {
    content: "...";
    width: 40px;
    height: 100%;
    display: inline-block;
    position: absolute;
    right: 0;
    top: 0;
    text-align: center;
    background: black;
    color: #fff;
    font-size: 1em;
    line-height: 2em;
  }
  &.true {
    &:after {
      content: "⌃";
      font-size: 1.5em;
      line-height: 1.8em;
    }
  }
`;
const StyledOptionsNetworkDropdown = styled.div`
  display: none;
  padding: 0;
  border-radius: 0.125em;
  background: #fff;
  border: 0.071em solid rgba(34, 36, 38, 0.15);
  position: absolute;
  left: 0;
  width: 100%;
  max-width: 18em;
  &.true {
    display: block;
  }
  button {
    box-shadow: none;
    color: #000;
    width: 100%;
    text-align: left;
    padding-left: 1em;
    animation: 0.25s slidein forwards;
    position: relative;
    background: none;
    left: -5px;
    &:nth-child(2) {
      animation-duration: 0.25s;
      animation-delay: 0.05s;
    }
  }
  @keyframes slidein {
    from {
      left: -5px;
    }

    to {
      left: 0;
    }
  }
`;

const StyledInfoModalh2 = styled.h2`
  color: rgb(46 41 66);
  border-bottom: solid 0.125em #f9d869;
  padding-bottom: 1em;
  text-align: center;
`;

const StyledCenterButton = styled.button`
  margin: auto;
`;

const StyledOrderedList = styled.ol`
  padding-left: 1.2em;
  li {
    list-style: auto;
  }
`;

const StyledH1 = styled.h1``;
const StyledTextarea = styled.textarea`
  min-height: 6.4285em;
  overflow: hidden;
  transition: all 0.15s ease;
  border: none;
`;
const StyledLabel = styled.label``;
const StyledSelect = styled.select``;
const StyledInput = styled.input``;
const InputStyling = styled.div`
  margin: 0 0 1em;
  position: relative;
`;
const EditStyledLabel = styled.label`
  background: #eeeeee;
  padding: 0 0.5rem;
`;
const PostPreviewLabelStyling = styled.div`
  display: block;
  margin: 0 0 0.28571429rem 0;
  color: rgba(0, 0, 0, 0.87);
  font-size: 0.92857143em;
  font-weight: 700;
  text-transform: none;
  padding: 0 0.5rem;
  background: #eeeeee;
  span {
    cursor: pointer;
    float: right;
    @media screen and (max-width: 40em) {
      display: none;
    }
  }
`;
const PostPreviewStyling = styled.div`
  padding: 1em;
  margin-bottom: 10px;
  background: #fff;
  border: none;
  outline: 0;
  color: rgba(0, 0, 0, 0.87);
  border-radius: 0.28rem;
  &.markdownPreview {
    font-family: charter, Georgia, Cambria, "Times New Roman", Times, serif;
    a {
      font-weight: normal;
    }
    h1 {
      margin-bottom: 1em;
      font-weight: normal;
      line-height: 1.65em;
      margin-top: 0;
    }
    h2,
    h3,
    h4,
    h5,
    h6 {
      font-family: Lato, "Helvetica Neue", Arial, Helvetica, sans-serif;
      margin-bottom: 1.5em;
    }
    p {
      text-size-adjust: 100%;
      line-height: 1.6285em;
      margin: 0px 0em 2.05em;
      letter-spacing: -0.003em;
    }
    ul,
    ol {
      list-style: inherit;
      margin-bottom: 1em;
      li {
        margin-left: 1.2em;
      }
    }

    i,
    em {
      font-style: italic;
    }
  }
`;
const StyledButton = styled.button`
  margin: 2em auto;
  &.added-xdai {
    background: #000 url("/Flat_tick_icon.svg") center no-repeat;
    box-shadow: none;
    background-size: 20px;
    background-position: 96% center;
    padding-right: 3em;
    &:hover {
      color: #fff;
    }
  }
`;

const InfoStyling = styled.div`
  padding: 0.5rem 0;
  text-align: center;
`;
const PostAreaStyling = styled.div`
  padding: 2.5rem 0 1rem;
  width: 100%;
`;

const ErrorMessageAreaStyling = styled.div`
  padding: 1rem 1.5rem;
  font-size: 120%;
  width: 100%;
  border: none;
  top: 0;
  left: 0;
  background: #ffffff;
  word-wrap: break-word;
`;

const MessageTitleAreaStyling = styled.div`
  width: 100%;
  border-bottom: none;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 0;
  left: 0;
  background: #ffffff;
`;

const MessageButtonStyling = styled.div`
  padding: 0;
  display: block;
  flex-direction: column;
  justify-content: right;
  align-items: right;
  button {
    margin: auto;
    &.blue-btn {
      box-shadow: inset 0 0 0 35.7142em rgb(46, 41, 66);
      &:hover {
        background: #ffffff;
        box-shadow: inset 0 0 0 0.142em rgb(46, 41, 66);
        color: #1b1c1d;
      }
    }
  }
  .text-center {
    text-align: center;
  }
`;

const FlexBox = styled.div`
  display: flex;
  width: 100%;
  > div {
    flex: 1;
    button {
      display: inline-block;
    }
  }
  .padding-t-1 {
    padding-top: 1em;
  }
  .text-right {
    text-align: right;
  }
`;

const MessageAreaStyling = styled.div`
  padding: 1rem 1.5rem;
  min-height: 100px;
  min-width: 300px;
  font-size: 120%;
  width: 100%;
  border-bottom: 0.07142em solid #eaeaea;
  top: 0;
  left: 0;
  background: #ffffff;
  word-wrap: break-word;
`;
