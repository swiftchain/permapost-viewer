import { useState, useEffect } from "react";

import Cookies from "js-cookie";

import Link from "next/link";

import styled from "styled-components";
import ReactMarkdown from "react-markdown";
import gfm from "remark-gfm";

import Layout from "../components/Layout";

import useScript from "../lib/useScript";

import CONTRACT_ABI from "../lib/abi_2021_02_25.json";

import Modal from "../components/Modal";

const DEFAULT_BLOCKCHAIN = Object.entries(process.env.BLOCKCHAINS)[1][0];
const DEFAULT_SUPPORTED_BLOCKCHAINS = Object.entries(process.env.BLOCKCHAINS);
const DEFAULT_BLOCKCHAIN_NAME = Object.entries(process.env.BLOCKCHAINS)[1][1];

export default function Post() {
  const [web3, setWeb3] = useState(null);
  const [contract, setContract] = useState(null);
  const [walletAddress, setWalletAddress] = useState(null);
  const [ethPerUsd, setEthPerUsd] = useState(0);

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [postText, setPostText] = useState("");
  const [burnAmount, setBurnAmount] = useState(0);
  const [gasProps, setGasProps] = useState({
    gas: 91860,
    gasPrice: 1000000000,
  });
  const [burnAmountUsd, setBurnAmountUsd] = useState(0);
  const [donateAmount, setDonateAmount] = useState(0);
  const [createdPostTxHash, setCreatedPostTxHash] = useState("");
  const [selectedBlockchain, setSelectedBlockchain] = useState("");
  const [createdPostBlockChain] = useState(DEFAULT_BLOCKCHAIN);
  const [createdPostSetBlockChains] = useState(DEFAULT_SUPPORTED_BLOCKCHAINS);
  const [modal1Visible, setModal1Visible] = useState(false);
  const [modal2Visible, setModal2Visible] = useState(false);
  const [modal3Visible, setModal3Visible] = useState(false);
  const [modal4Visible, setModal4Visible] = useState(false);
  const [modal5Visible, setModal5Visible] = useState(false);
  const [modal6Visible, setModal6Visible] = useState(false);
  const [metamaskErrorModalVisible, setMetamaskErrorModalVisible] =
    useState(false);
  const [suggestxDaiModalVisible, setSuggestxDaiModalVisible] = useState(false);
  const [unsupportedChainModalVisible, setUnsupportedChainModalVisible] =
    useState(false);

  const [textAreaRows, setTextAreaRows] = useState(0);

  const [postStatus, setPostStatus] = useState("");
  const [posting, setPosting] = useState(false);

  // we need to load the web3 script like this due to server side rendering during build
  const web3LoadStatus = useScript(
    "https://cdn.jsdelivr.net/npm/web3@latest/dist/web3.min.js"
  );

  let gas = { gas: 91860, gasPrice: 1000000000 };

  let supportedBlockchains = null;

  let firstTimeMessages = {
    message1: (
      <MessageAreaStyling>
        <div>
          {`Welcome ${walletAddress}, we haven’t seen you post on here before.
          Since we don’t know who you are by name, when we refer to “you” in any
          text, we’re referring to ${walletAddress}. We really appreciate you
          having a look around and hope you find great value in Permapost.
          Before you proceed, you should know that posting on the blockchain
          comes with a few warnings. We want to make sure you are aware of these
          warnings.`}
        </div>
      </MessageAreaStyling>
    ),
    message2: (
      <div>
        <MessageAreaStyling>
          {`If you are familiar with how the blockchain works, you’ll know that
          any transaction on the blockchain is permanent, and can never be
          modified or removed unless the blockchain it’s on stops working or is
          taken down somehow. This is very improbable and will most likely not
          happen.`}
        </MessageAreaStyling>
      </div>
    ),
    messaeg3: (
      <div>
        <MessageAreaStyling>
          {`That being said, we don’t want to scare you away, it’s just important
          that you’re aware of the risks. Firstly, since our platform is
          decentralised, unregulated, public and unkillable, you will have to
          understand that whatever you post on here can never be removed or
          deleted in any way. Although an interface like Permapost could one day
          hide posts, they will still persist as public data on the blockchain.`}
        </MessageAreaStyling>
      </div>
    ),
    message4: (
      <div>
        <MessageAreaStyling>
          {`We would like to take the opportunity to ask that you accept the risks
          of posting here:`}
          <ul>
            <li>
              {`${"\u2022"} Our content is public and available for all to see.`}
            </li>
            <li>{`${"\u2022"} Your posts will be stored on a blockchain.`}</li>
            <li>
              {`${"\u2022"} Your post will create a transaction on the blockchain
              and its content will be stored inside the transaction’s data.`}
            </li>
            <li>
              {`${"\u2022"} Your post is permanent and cannot be removed or
              modified.`}
            </li>
            <li>{`${"\u2022"} You will be known only as ${walletAddress}.`}</li>
            <li>
              {`${"\u2022"} Unless you make yourself publicly known by stating your
              name on the post, no additional information will be stored in
              terms of your identity.`}
            </li>
            <li>
              {`${"\u2022"} You should know that, ultimately, there may be ways to
              track the wallet address back to you, although this is very
              difficult and there are no easy ways to perform this action.`}
            </li>
            <li>
              {`${"\u2022"} The fact that you accepted these terms will be stored
              as your first post on the blockchain, as proof that we made you
              aware of the risks.`}
            </li>
          </ul>
        </MessageAreaStyling>
      </div>
    ),
    message5: (
      <div>
        <MessageAreaStyling>
          {`The developers of Permapost reserve the right to deploy future
          versions of the platform that hides your content from the public eye.
          We resist this move ideologically but may face legal or other pressure
          that forces our hand.`}
        </MessageAreaStyling>
      </div>
    ),
    message6: (
      <div>
        <MessageAreaStyling>
          {`Okay, ${walletAddress}, all done now. You’re free to post on Permapost.
          Happy posting!`}
        </MessageAreaStyling>
      </div>
    ),
  };

  useEffect(() => {
    // only init the web3 object once, and only if the script has been loaded
    if (typeof window != "undefined" && web3LoadStatus === "ready" && !web3) {
      (async () => {
        console.log("Startup, test eth_requestAccounts");

        let testPassed = false;
        try {
          setMetamaskErrorModalVisible(false);
          const sendTest = await window.ethereum.send("eth_requestAccounts");
          console.log("sendTest", sendTest);
          testPassed = true;
        } catch (error) {
          setMetamaskErrorModalVisible(true);
          console.log("sendTest Error: ", error);
        }

        if (testPassed) {
          console.log("updating web3");
          const newWeb3 = new window.Web3(window.ethereum);
          console.log("newWeb3", newWeb3);
          const accounts = await newWeb3.eth.getAccounts();
          console.log("accounts", accounts);
          setWalletAddress(accounts[0]);
          setWeb3(newWeb3);
        }
      })();
    }
  }, [web3LoadStatus, web3, walletAddress]);

  useEffect(() => {
    // only initialise the contract once, and only if web3 has been initialised.
    let new_contract = null;
    if (web3 && walletAddress) {
      web3.eth.getChainId().then((chainID) => {
        // Detect which blockchain MM is connected to. ID 1 means Ethereum, ID 100 means xDai.
        if (chainID == 1) {
          console.log("Ethereum network detected in MM.");
          setSelectedBlockchain("ethereum");
          // If on Ethereum, suggest xDai to lower fees.
          setSuggestxDaiModalVisible(true);
          new_contract = new web3.eth.Contract(
            CONTRACT_ABI,
            process.env.ETH_CONTRACT_ADDRESS
          );
        } else if (chainID == 100) {
          console.log("xDai network detected in MM.");
          setSelectedBlockchain("xdai");
          setSuggestxDaiModalVisible(false);
          new_contract = new web3.eth.Contract(
            CONTRACT_ABI,
            process.env.XDAI_CONTRACT_ADDRESS
          );
        } else {
          // Instruct user to select a supported blockchain.
          setSelectedBlockchain("Unsupported Network");
          setUnsupportedChainModalVisible(true);
          supportedBlockchains = createdPostSetBlockChains;
          return;
        }
        setContract(new_contract);
        (async () => {
          const ethGasEstimate = await new_contract.methods
            .EthPrice()
            .estimateGas();
          let retrievedGasPrice = 0;
          web3.eth.getGasPrice(function (e, r) {
            retrievedGasPrice = r;
          });
          console.log("ethGasEstimate", ethGasEstimate);

          const ethPrice = await new_contract.methods.EthPrice().call({
            from: walletAddress,
          });
          console.log("ethPrice", ethPrice);

          const ethToUsd = await new_contract.methods
            .ethToUsd(1)
            .call({ from: walletAddress });
          console.log("ethToUsd", ethToUsd);
          const new_ethPerUsd = ethToUsd / 10 ** 18;
          console.log("new_ethPerUsd", new_ethPerUsd);
          setEthPerUsd(new_ethPerUsd);
          if (selectedBlockchain == "xdai") {
            setGasProps({ gas: 91860, gasPrice: 1000000000 });
          } else if (selectedBlockchain == "ethereum") {
            //setGasProps({ gas: ethGasEstimate, gasPrice: retrievedGasPrice });
            setGasProps("");
          }
        })();

        console.log("new_contract", new_contract);
        console.log("new_contract.methods", new_contract.methods);
        new_contract.defaultAccount = walletAddress;
      });
    }
  }, [web3, walletAddress]);

  const readVisitCookie = () => {
    const visited = Cookies.get("visited");
    if (visited) {
      return true;
    } else {
      return false;
    }
  };

  function createVisitCookie() {
    Cookies.set("visited", "visitedTrue", { expires: 365 });
  }

  // This is executed before a post is made to ensure user has accepted the risks.
  const risksAccepted = () => {
    if (readVisitCookie() == false) {
      setModal1Visible(true);
    } else {
      createPost();
    }
  };

  // make a smokesignal post to the blockchain
  const createPost = async () => {
    setPosting(true);
    setPostStatus("Post Mining...");
    const smokesignal = {
      m: [title, description, postText],
      v: 3,
      c: { topic: "Permapost" },
    };
    console.log("smokesignal", smokesignal);

    // Run the burnMessage function from the contract
    // only include burn amount and donate amount if the burnAmount is set
    try {
      const postReceipt = burnAmount
        ? await contract.methods
            .burnMessage(
              `!smokesignal${JSON.stringify(smokesignal)}`,
              donateAmount
            )
            .send({
              from: walletAddress,
              value: burnAmount,
              gas: gasProps.gas,
              gasPrice: gasProps.gasPrice,
            })
        : await contract.methods
            .burnMessage(`!smokesignal${JSON.stringify(smokesignal)}`, 0)
            .send({
              from: walletAddress,
              gas: gasProps.gas,
              gasPrice: gasProps.gasPrice,
            });

      console.log("postReceipt", postReceipt);

      // retrieve the new post's tx hash from the post receipt to allow the
      // user to easily view their new post
      const txHash = postReceipt?.transactionHash;
      console.log("txHash", txHash);
      setCreatedPostTxHash(txHash);
      setPosting(true);
      setPostStatus("Post Ready");
      console.log(273, postStatus);
    } catch (error) {
      setPostStatus("Failed to post.");
      setTimeout(() => {
        setPosting(false);
      }, 2000);
    }
  };

  const resetForm = () => {
    setTitle("");
    setDescription("");
    setPostText("");
    setCreatedPostTxHash("");
    setTextAreaRows(0);
    setPostStatus("");
  };

  // handle burn amount input field changes
  const handleBurnAmountChange = (event) => {
    const new_burn = event.target.value;
    if (new_burn < donateAmount) {
      setDonateAmount(new_burn);
    }
    setBurnAmount(new_burn);
  };

  return (
    <Layout title="Create a Permapost">
      <Modal show={posting}>
        {!createdPostTxHash ? (
          <h2>{postStatus}</h2>
        ) : (
          <div>
            <h2>Post Ready</h2>
            If you would like to view your new permapost, please{" "}
            <a
              target="_blank"
              rel="noreferrer"
              href={`/view?blockchain=${selectedBlockchain}&tx=${createdPostTxHash}`}
            >
              click this link
            </a>
            .
            <FlexBox>
              <div className="text-right">
                <button onClick={() => setPosting(false)}>Close</button>
              </div>
            </FlexBox>
          </div>
        )}
      </Modal>
      <Modal show={metamaskErrorModalVisible}>
        <MessageTitleAreaStyling>
          <h2>MetaMask not found</h2>
        </MessageTitleAreaStyling>
        <div>
          <ErrorMessageAreaStyling>
            {`Permapost requires MetaMask with a configured wallet to be installed before posts can be made.`}{" "}
            <br></br>
            {`The MetaMask extension can be found here:`}{" "}
            <Link href="https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en">
              <a target="_blank">{`MetaMask`}</a>
            </Link>
          </ErrorMessageAreaStyling>
        </div>
        <MessageButtonStyling>
          <button
            onClick={() => {
              window.location.reload();
            }}
          >
            Reload
          </button>
        </MessageButtonStyling>
      </Modal>

      <Modal
        show={suggestxDaiModalVisible}
        hideModal={() => setSuggestxDaiModalVisible(false)}
      >
        <MessageTitleAreaStyling>
          <h2>Blockchain Suggestion</h2>
        </MessageTitleAreaStyling>
        <div>
          <ErrorMessageAreaStyling>
            {`In order to pay much lower fees, we suggest using the xDai network instead of Ethereum.`}{" "}
            <br></br>
            {`More information can be found here:`}{" "}
            <Link href="https://www.xdaichain.com/for-users/wallets/metamask/metamask-setup">
              <a target="_blank">{`xDai`}</a>
            </Link>
          </ErrorMessageAreaStyling>
        </div>
      </Modal>

      <Modal show={unsupportedChainModalVisible}>
        <MessageTitleAreaStyling>
          <h2>Unsupported Blockchain Selected</h2>
        </MessageTitleAreaStyling>
        <div>
          <ErrorMessageAreaStyling>
            {`Please select a supported blockchain network in MetaMask.`}{" "}
            <br></br>
            <br></br>
            {`Currently supported networks:`}
            {createdPostSetBlockChains.map((chain, i) => (
              <li key={chain.id}>{chain[1]}</li>
            ))}
          </ErrorMessageAreaStyling>
          <MessageButtonStyling>
            <button
              onClick={() => {
                window.location.reload();
              }}
            >
              Reload
            </button>
          </MessageButtonStyling>
        </div>
      </Modal>

      <Modal show={modal1Visible} hideModal={() => setModal1Visible(false)}>
        <MessageTitleAreaStyling>
          <h2>Welcome</h2>
        </MessageTitleAreaStyling>
        <div>{firstTimeMessages.message1}</div>
        <MessageButtonStyling>
          <button
            onClick={() => {
              setModal1Visible(false);
              setModal2Visible(true);
            }}
          >
            Next
          </button>
        </MessageButtonStyling>
      </Modal>
      <Modal show={modal2Visible} hideModal={() => setModal2Visible(false)}>
        <MessageTitleAreaStyling>
          <h2>Welcome</h2>
        </MessageTitleAreaStyling>
        <div>{firstTimeMessages.message2}</div>
        <MessageButtonStyling>
          <button
            onClick={() => {
              setModal2Visible(false);
              setModal3Visible(true);
            }}
          >
            Next
          </button>
        </MessageButtonStyling>
      </Modal>
      <Modal show={modal3Visible} hideModal={() => setModal3Visible(false)}>
        <MessageTitleAreaStyling>
          <h2>Welcome</h2>
        </MessageTitleAreaStyling>
        <div>{firstTimeMessages.messaeg3}</div>
        <MessageButtonStyling>
          <button
            onClick={() => {
              setModal3Visible(false);
              setModal4Visible(true);
            }}
          >
            Next
          </button>
        </MessageButtonStyling>
      </Modal>
      <Modal show={modal4Visible} hideModal={() => setModal4Visible(false)}>
        <MessageTitleAreaStyling>
          <h2>Welcome</h2>
        </MessageTitleAreaStyling>
        <div>{firstTimeMessages.message4}</div>
        <MessageButtonStyling>
          <button
            onClick={() => {
              setModal4Visible(false);
              setModal5Visible(true);
            }}
          >
            Next
          </button>
        </MessageButtonStyling>
      </Modal>
      <Modal show={modal5Visible} hideModal={() => setModal5Visible(false)}>
        <MessageTitleAreaStyling>
          <h2>Welcome</h2>
        </MessageTitleAreaStyling>
        <div>{firstTimeMessages.message5}</div>
        <MessageButtonStyling>
          <button
            onClick={() => {
              setModal5Visible(false);
              setModal6Visible(true);
            }}
          >
            Next
          </button>
        </MessageButtonStyling>
      </Modal>
      <Modal show={modal6Visible} hideModal={() => setModal6Visible(false)}>
        <MessageTitleAreaStyling>
          <h2>Welcome</h2>
        </MessageTitleAreaStyling>
        <div>{firstTimeMessages.message6}</div>
        <MessageButtonStyling>
          <button
            onClick={() => {
              createVisitCookie();
              createPost();
              setModal6Visible(false);
            }}
          >
            Accept
          </button>
        </MessageButtonStyling>
      </Modal>

      <StyledH1>Create a Permapost</StyledH1>

      <InfoStyling>
        If you would like to view a permapost, please{" "}
        <Link href="/view/">
          <a>click this link</a>
        </Link>{" "}
        and enter the transaction hash
      </InfoStyling>

      <InfoStyling>
        If you would like to view an example permapost, please{" "}
        <Link
          href={{
            pathname: "/view/",
            query: {
              blockchain: "eth",
              tx: "0x0e70fc6b53d8dbaa8563e062de35eadf84af57228a6ac3a247550231c2ee2d73",
            },
          }}
        >
          <a>click this link</a>
        </Link>
      </InfoStyling>

      <InfoStyling>
        To make a new permapost, just supply your post below and click on post
        to your chosen blockchain. This app is known to work with Metamask
      </InfoStyling>

      <PostAreaStyling>
        <InputStyling>
          <StyledLabel htmlFor="title">Title</StyledLabel>
          <StyledInput
            name="title"
            value={title}
            placeholder="Title"
            onChange={(event) => setTitle(event.target.value)}
          />
        </InputStyling>
        <InputStyling>
          <StyledLabel htmlFor="description">Description</StyledLabel>
          <StyledInput
            name="description"
            value={description}
            placeholder="Description"
            onChange={(event) => setDescription(event.target.value)}
          />
        </InputStyling>
        <InputStyling>
          <StyledLabel htmlFor="content">Content</StyledLabel>
          <StyledTextarea
            style={{ height: textAreaRows + "px" }}
            rows={40}
            cols={150}
            value={postText}
            onChange={(event) => {
              setTextAreaRows(
                event.target.value.length === 0 ? 0 : event.target.scrollHeight
              );
              setPostText(event.target.value);
            }}
            name="content"
            placeholder="Heading&#10;=======&#10;Paragraphs are separated by a blank line."
          />
        </InputStyling>
        {postText && (
          <PostPreviewLabelStyling>Post preview</PostPreviewLabelStyling>
        )}
        {postText && (
          <PostPreviewStyling>
            <ReactMarkdown remarkPlugins={[gfm]}>{postText}</ReactMarkdown>
          </PostPreviewStyling>
        )}
        <InputStyling>
          <StyledLabel htmlFor="chain">Blockchain</StyledLabel>
          <StyledInput
            name="chain"
            value={`Connected Wallet: ${selectedBlockchain}`}
            readOnly
            onChange={() => {}}
          />
        </InputStyling>
        {!createdPostTxHash ? (
          <StyledButton
            className={
              title.length > 0 && postText.length > 0
                ? "clickable"
                : "clickable-inactive"
            }
            onClick={risksAccepted}
          >
            Post
          </StyledButton>
        ) : (
          <div>
            <StyledButton onClick={resetForm}>Reset the form</StyledButton>
            <InfoStyling>
              If you would like to view your new permapost, please{" "}
              <Link
                href={{
                  pathname: "/view/",
                  query: {
                    blockchain: selectedBlockchain,
                    tx: createdPostTxHash,
                  },
                }}
              >
                <a>click this link</a>
              </Link>
            </InfoStyling>
          </div>
        )}
      </PostAreaStyling>
    </Layout>
  );
}

const StyledH1 = styled.h1``;
const StyledTextarea = styled.textarea`
  min-height: 6.4285em;
  overflow: hidden;
  transition: all 0.15s ease;
`;
const StyledLabel = styled.label``;
const StyledSelect = styled.select``;
const StyledInput = styled.input``;
const InputStyling = styled.div`
  margin: 0 0 1em;
`;
const PostPreviewLabelStyling = styled.div`
  display: block;
  margin: 0 0 0.28571429rem 0;
  color: rgba(0, 0, 0, 0.87);
  font-size: 0.92857143em;
  font-weight: 700;
  text-transform: none;
`;
const PostPreviewStyling = styled.div`
  padding: 1em;
  margin-bottom: 10px;
  background: #fff;
  border: 0.071em solid rgba(34, 36, 38, 0.15);
  outline: 0;
  color: rgba(0, 0, 0, 0.87);
  border-radius: 0.28rem;
`;
const StyledButton = styled.button`
  margin: 2em auto;
`;
const InfoStyling = styled.div`
  padding: 0.5rem 0;
  text-align: center;
`;
const PostAreaStyling = styled.div`
  padding: 2.5rem 0 1rem;
  width: 100%;
`;

const MessageAreaStyling = styled.div`
  padding: 1rem 1.5rem;
  min-height: 300px;
  min-width: 300px;
  font-size: 120%;
  width: 100%;
  border-bottom: 0.07142em solid #eaeaea;
  top: 0;
  left: 0;
  background: #ffffff;
  word-wrap: break-word;
`;

const ErrorMessageAreaStyling = styled.div`
  padding: 1rem 1.5rem;
  font-size: 120%;
  width: 100%;
  border-bottom: 0.07142em solid #eaeaea;
  top: 0;
  left: 0;
  background: #ffffff;
  word-wrap: break-word;
`;

const MessageTitleAreaStyling = styled.div`
  width: 100%;
  border-bottom: 0.07142em solid #eaeaea;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 0;
  left: 0;
  background: #ffffff;
`;

const MessageButtonStyling = styled.div`
  padding: 0;
  display: flex;
  flex-direction: column;
  justify-content: right;
  align-items: right;
`;

const FlexBox = styled.div`
  display: flex;
  width: 100%;
  > div {
    flex: 1;
    button {
      display: inline-block;
    }
  }
  .text-right {
    text-align: right;
  }
`;
