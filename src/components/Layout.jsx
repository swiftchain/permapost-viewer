import { useState, useEffect } from "react";
import Head from "next/head";
import Link from "next/link";
import styled from "styled-components";
import Nav from "./Nav";

export default function Layout({
  title = "Permapost",
  description = "Create a permanent post.",
  children = null,
  pageWidthLimit = "46.4285em",
}) {
  useEffect(() => {
    setTimeout(() => {
      document.querySelector(".screen-loader").classList.add("hide-loader");
    }, 1000);
  });
  const [pageWidth, setPageWidth] = useState(pageWidthLimit);
  return (
    <LayoutStyling>
      <div className="screen-loader">
        <div className="screen-loader--text">Permapost</div>
        <div className="screen-loader--loading"></div>
      </div>
      <Head>
        <title>{title}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.svg" />

        <meta name="title" content={title} key="pagetitle" />
        <meta name="description" content={description} key="pagedescription" />

        <meta property="og:title" content={title} key="ogtitle" />
        <meta
          property="og:description"
          content={description}
          key="ogdescription"
        />
        <meta
          property="og:image"
          content="https://www.permapost.io/Foundry-with-background.png"
        />
      </Head>

      <StyledHeader>
        <Nav />
      </StyledHeader>

      <StyledMain pageWidth={pageWidth}>{children}</StyledMain>

      <StyledFooter>
        &copy; Copyright {new Date().getFullYear()}, Permapost
      </StyledFooter>
    </LayoutStyling>
  );
}

const StyledHeader = styled.header`
  width: 100%;
  border-bottom: 0.07142em solid #eaeaea;
  display: flex;
  position: sticky;
  top: 0;
  left: 0;
  background: #ffffff;
  z-index: 2;
`;

const RowsStyling = styled.div`
  display: inline-block;
  width: 100%;
  padding: 0;
  margin: 0;
`;
const RowStyling = styled.div`
  display: inline-block;
  width: 30%;
  padding: 0;
  margin: 0;
`;
const RowTwoStyling = styled.div`
  display: inline-block;
  width: 40%;
  text-align: center;
  padding: 0;
  margin: 0;
  position: absolute;
  top: 0;
`;

const StyledFooter = styled.footer`
  width: 100%;
  height: 3.5714285714em;
  border-top: 0.07142em solid #eaeaea;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const StyledMain = styled.main`
  padding: 0 1rem 1rem;
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  max-width: ${(props) => props.pageWidth};
  width: 100%;
`;
const LayoutStyling = styled.div`
  min-height: 100vh;
  padding: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
