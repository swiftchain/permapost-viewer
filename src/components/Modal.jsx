import { useState } from "react";

import styled from "styled-components";

const Modal = ({ show, children = null, hideModal }) => {
  return (
    <ModalStyling className={show ? "show-post-detail" : ""}>
      <ModalInner>
        {children}
        {hideModal && (
          <ModalCloseButton onClick={hideModal}>X</ModalCloseButton>
        )}
      </ModalInner>
    </ModalStyling>
  );
};

export default Modal;

const ModalInner = styled.div`
  border-radius: 0.2142em;
  background: #ffffff;
  padding: 1rem 2rem;
  margin-top: 3rem;
  margin-bottom: 3rem;
  font-size: 0.8571em;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translateX(-50%) translateY(-50%);
  width: 96%;
  max-height: 100vh;
  max-width: 650px;
`;

const ModalStyling = styled.div`
  background: rgba(234, 234, 234, 0.95);
  position: fixed;
  top: 0;
  left: 0;
  display: none;
  width: 100%;
  height: 100%;
  z-index: 11;
  &.show-post-detail {
    display: block;
  }
`;

const ModalCloseButton = styled.button`
  position: absolute;
  top: -16px;
  right: -4px;
  padding: 10px 13px;
  width: auto;
  border-bottom-right-radius: 0;
`;
