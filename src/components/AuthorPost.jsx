import { useEffect, useState } from "react";
import styled, { keyframes } from "styled-components";
import copyToClipBoard from "./../helpers/copyToClipBoard";

import CONTRACT_ABI from "../lib/abi_2021_02_25.json";

const abiDecoder = require("abi-decoder");
const ABI = require("./../lib/abi_2021_02_25.json");
abiDecoder.addABI(ABI);

export default function AuthorPost({ xDaiWeb3, ethWeb3, item }) {
  const [copiedVisible, setCopiedVisible] = useState(false);
  const [tipAmount, setTipAmount] = useState(0);
  const copyUrl = (url) => {
    if (copyToClipBoard(url)) {
      setCopiedVisible(true);
      setTimeout(() => {
        setCopiedVisible(false);
      }, 2000);
    }
  };
  useEffect(() => {
    getTip();
  });
  async function getTip() {
    try {
      let new_contract = null;
      let hash = "";
      let getTips = 0;

      if (item.blockchain == "eth") {
        const tx_receipt = await ethWeb3?.eth?.getTransactionReceipt(item.hash);
        const tx_logs = await abiDecoder.decodeLogs(tx_receipt?.logs);
        const tx_log_0 = tx_logs?.[0];
        tx_log_0?.events?.forEach?.((event) => {
          switch (event.name) {
            case "_hash": {
              hash = event.value;
              break;
            }
          }
        });
        new_contract = await new ethWeb3.eth.Contract(
          CONTRACT_ABI,
          process.env.ETH_CONTRACT_ADDRESS
        );
        getTips = await new_contract.methods.storedMessageData(hash).call();
        setTipAmount(
          parseFloat(ethWeb3?.utils?.fromWei(getTips.dollarsTipped))
        );
      } else if (item.blockchain == "xdai") {
        const tx_receipt = await xDaiWeb3?.eth?.getTransactionReceipt(
          item.hash
        );
        const tx_logs = await abiDecoder.decodeLogs(tx_receipt?.logs);
        const tx_log_0 = tx_logs?.[0];
        tx_log_0?.events?.forEach?.((event) => {
          switch (event.name) {
            case "_hash": {
              hash = event.value;
              break;
            }
          }
        });
        new_contract = await new xDaiWeb3.eth.Contract(
          CONTRACT_ABI,
          process.env.XDAI_CONTRACT_ADDRESS
        );
        getTips = await new_contract.methods.storedMessageData(hash).call();
        setTipAmount(
          parseFloat(xDaiWeb3?.utils?.fromWei(getTips.dollarsTipped))
        );
      }
    } catch (error) {
      console.error(error);
    }
  }
  return (
    <FlexBottom>
      <div>
        <CopyButton className="tips" title="Tips received">
          {tipAmount >= 0.01 ? (
            <span>
              <strong>$</strong>
              {tipAmount.toFixed(2)}
            </span>
          ) : (
            <span>{tipAmount === "" ? `...` : `No tips.`}</span>
          )}
        </CopyButton>{" "}
      </div>
      <div>
        {copiedVisible && <CopiedSpan>Copied to clipboard!</CopiedSpan>}
        <CopyButton
          onClick={() =>
            copyUrl(
              `${document.location.origin}/view?blockchain=${item.blockchain}&tx=${item.hash}`
            )
          }
          title="Copy link to clipboard"
        ></CopyButton>
      </div>
    </FlexBottom>
  );
}

const keyFrameFadeIn = keyframes`
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
`;

const CopiedSpan = styled.span`
  position: absolute;
  right: 3em;
  animation-name: ${keyFrameFadeIn};
  animation-duration: 0.25s;
  animation-iteration-count: 1;
  animation-fill-mode: forwards;
`;

const CopyButton = styled.button`
  height: 25px;
  width: 25px;
  background: url(/clipboard-check.svg) no-repeat center;
  box-shadow: none;
  background-size: 25px;
  display: inline-block;
  transition: all 0.15s eae;
  padding: 0;
  position: relative;
  &:hover {
    background: url(/clipboard-check.svg) no-repeat center;
    background-size: contain;
    box-shadow: none;
    opacity: 0.7;
  }
  &.tips {
    background: url(/piggy-bank.svg) no-repeat center left;
    padding-left: 2.5em;
    background-size: 25px;
    text-align: right;
    color: rgba(41, 41, 41, 1);
    width: unset;
    font-size: 1em;
    font-weight: normal;
    color: rgba(41, 41, 41, 1);
    &:hover {
      cursor: default;
      background: url(/piggy-bank.svg) no-repeat center left;
      background-size: 25px;
    }
  }
`;

const FlexBottom = styled.div`
  display: flex;
  margin-top: 1.3em;
  margin-bottom: 0.5em;
  align-items: center;
  justify-content: center;
  color: rgba(41, 41, 41, 1);
  > div {
    flex: 1;
    font-size: 0.6666666667em;
    color: rgba(41, 41, 41, 1);
    &:last-child {
      text-align: right;
    }
  }
`;
