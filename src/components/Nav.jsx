import { useState } from "react";
import styled from "styled-components";
import Link from "next/link";
import Menu from "./Menu";

export default function Nav() {
  const [toggleMobileMenu, setToggleMobileMenu] = useState(false);
  return (
    <StyledNav>
      <StyledMenu toggleMobileMenu={toggleMobileMenu}>
        <li className="logo">
          <StyledLogoText>
            <Link href="/">
              <a>
                <StyledLogo src="/favicon.svg" alt="Permapost" />
              </a>
            </Link>
          </StyledLogoText>
          <StyledHamburgerMenu
            toggleMobileMenu={toggleMobileMenu}
            onClick={() => setToggleMobileMenu(!toggleMobileMenu)}
          >
            <span></span>
            <span></span>
            <span></span>
          </StyledHamburgerMenu>
        </li>
        <li className="home-link item">
          <Link href="https://permapost.io/">
            <a>Home/About</a>
          </Link>
        </li>
        <li className="item">
          <Link href="/">
            <a>Create a permapost</a>
          </Link>
        </li>
        <li className="item">
          <Link href="/view?blockchain=xdai&tx=0x542a75c4fd23e5bdd377bb096af06976ff58410660fd1ef1cf7f0bc5b2d494c9">
            <a>Example: The Ethics of Uncensorability</a>
          </Link>
        </li>
        <li className="item dev-links">
          <Menu />
        </li>
      </StyledMenu>
    </StyledNav>
  );
}

const StyledDevLinks = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  @media screen and (max-width: 40em) {
    position: relative;
    display: ${(props) => (props.toggleMobileMenu ? "block" : "none")};
    text-align: right;
  }
`;

const StyledHamburgerMenu = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  height: 3.5em;
  width: 3.5em;
  background: none;
  cursor: pointer;
  span {
    display: block;
    height: 5px;
    width: 35px;
    border-radius: 3px;
    background: rgba(0, 0, 0, 0.87);
    position: absolute;
    left: 10px;
    top: 10px;
    transition: all 0.25s ease;
    &:nth-child(1) {
      transform: ${(props) =>
        props.toggleMobileMenu ? "rotateZ(45deg)" : "initial"};
      top: ${(props) => (props.toggleMobileMenu ? "25px" : "15px")};
    }
    &:nth-child(2) {
      top: 27px;
      display: ${(props) => (props.toggleMobileMenu ? "none" : "block")};
    }
    &:nth-child(3) {
      top: ${(props) => (props.toggleMobileMenu ? "25px" : "39px")};
      transform: ${(props) =>
        props.toggleMobileMenu ? "rotateZ(135deg)" : "initial"};
    }
  }
  @media screen and (min-width: 40em) {
    display: none;
  }
`;

const StyledLogo = styled.img`
  height: 3.5em;
  width: 3.5em;
  padding: 0.5714em;
`;

const StyledLogoText = styled.strong`
  font-weight: bold;
  a {
    top: 2px;
    position: relative;
    text-decoration: none;
  }
`;

const StyledNav = styled.nav`
  width: 100%;
  margin: auto;
  font-size: 18px;
  @media screen and (max-width: 40em) {
    font-size: 17px;
  }
`;

const StyledMenu = styled.ul`
  align-items: flex-start;
  flex-wrap: nowrap;
  background: none;
  justify-content: center;
  display: flex;
  > li {
    order: 1;
    position: relative;
    height: 3.5em;
    &:nth-child(2) {
      @media screen and (max-width: 40em) {
        border-top: solid 0.0625em #a6a7aa;
      }
    }
    &:last-child {
      @media screen and (max-width: 40em) {
        box-shadow: 0 9px 5px 1px rgb(0 0 0 / 20%);
      }
    }
    &:nth-child(3),
    &:nth-child(4) {
      padding: 0 1.5em;
      @media screen and (max-width: 40em) {
        padding: 0;
      }
    }
  }
  > li.item > a {
    display: block;
    padding: 0 0.8em;
    text-decoration: none;
    position: relative;
    text-align: center;
    top: 50%;
    transform: translateY(-50%);
    @media screen and (max-width: 40em) {
      text-align: right;
    }
  }
  li.logo {
    order: 0;
  }
  li.home-link {
    order: 1;
    flex: 1;
    text-align: left;
    @media screen and (max-width: 40em) {
      text-align: right;
    }
    > a {
      text-align: left;
      @media screen and (max-width: 40em) {
        text-align: right;
      }
    }
  }
  li.dev-links {
    flex: 1;
  }
  @media screen and (max-width: 40em) {
    display: block;
  }
  li.item {
    @media screen and (max-width: 40em) {
      display: ${(props) => (props.toggleMobileMenu ? "block" : "none")};
      text-align: right;
    }
  }
`;
