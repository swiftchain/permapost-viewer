import styled from "styled-components";
import { useState, useEffect } from "react";
import Link from "next/link";
import CONTRACT_ABI from "../lib/abi_2021_02_25.json";

export default function TipAuthor({ postHash = "", blockChainName = "" }) {
  const [mmInstalled, setMmInstalled] = useState(true);
  const [showTipForm, setShowTipForm] = useState(false);
  const [tipAmount, setTipAmount] = useState("");
  const [tipCurrency, setTipCurrency] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [tipStatus, setTipStatus] = useState("");
  const [currentChainID, setCurrentChainID] = useState("");
  const [userAddress, setUserAddress] = useState("");
  const [walletConnected, setWalletConnected] = useState(false);
  const [transactionHash, setTransactionHash] = useState(null);
  const [contract, setContract] = useState(null);

  //let currentChainID = "";
  //let userAddress = "";

  async function sendTip() {
    console.log("BlockchainNAME: ", blockChainName);
    console.log("PostHash: ", postHash);
    console.log("currentChainID", currentChainID);
    if (tipAmount === NaN || tipAmount <= 0.0) {
      setErrorMessage("Please enter a tip amount.");
      setTipStatus("");
      return;
    } else if (
      (blockChainName == "Ethereum" && currentChainID == 100) ||
      (blockChainName == "xDai" && currentChainID == 1)
    ) {
      setErrorMessage(
        "You need to be on the same network (" +
          blockChainName +
          ") to tip this permapost."
      );
      return;
    }
    setErrorMessage("");
    setTipStatus("");
    if (mmInstalled === false) {
      return;
    } else {
      await tipTransaction();
    }
  }

  const connectWallet = async () => {
    if (await checkWeb3Status()) {
      setShowTipForm(true);
    } else {
      setMmInstalled(false);
    }
  };

  useEffect(() => {
    if (walletConnected) {
      if (window.ethereum) {
        // Metamask account change
        window.ethereum.on("accountsChanged", function (accounts) {
          if (accounts.length > 0) {
            setAddress();
          } else {
            //setErrorMessage("User disconnected.");
            setShowTipForm(false);
            setTipStatus("");
            setErrorMessage("");
          }
        });
        // Network account change
        window.ethereum.on("chainChanged", function (networkId) {
          setNetwork();
        });
      } else {
        console.warn("No web3 detected.");
      }
    }
  });

  async function checkWeb3Status() {
    if (typeof window.ethereum !== "undefined") {
      setMmInstalled(true);
      await setAddress();

      await setNetwork();
      setWalletConnected(true);
      return true;
    } else {
      setMmInstalled(false);
      return false;
    }
  }

  async function setNetwork() {
    let web3 = new Web3(Web3.givenProvider);
    let new_contract = null;
    const chain = await web3.eth.getChainId().then((chainID) => {
      setCurrentChainID(chainID);

      if (chainID == 1) {
        setTipCurrency(" (ETH)");
        setTipStatus("");
        setErrorMessage("");
        new_contract = new web3.eth.Contract(
          CONTRACT_ABI,
          process.env.ETH_CONTRACT_ADDRESS
        );
      } else if (chainID == 100) {
        setTipCurrency(" (xDai)");
        setTipStatus("");
        setErrorMessage("");
        new_contract = new web3.eth.Contract(
          CONTRACT_ABI,
          process.env.XDAI_CONTRACT_ADDRESS
        );
      } else {
        setErrorMessage("Please select either the Ethereum or xDai network");
        setTipStatus("");
      }
      setContract(new_contract);
    });
  }

  async function setAddress() {
    let web3 = new Web3(Web3.givenProvider);
    const accounts = await ethereum.request({
      method: "eth_requestAccounts",
    });
    setUserAddress(accounts[0]);
  }

  const sleep = (milliseconds) => {
    return new Promise((resolve) => setTimeout(resolve, milliseconds));
  };

  async function tipTransaction() {
    let gasParams = {};
    let transactionParams = {};
    let web3 = new Web3(Web3.givenProvider);
    if (currentChainID == 100) {
      gasParams = { gasPrice: "1000000000" };

      let donateAmount = BigInt(Number(tipAmount) * 10 ** 18 * (1 / 100));

      await contract.methods
        .tipHashOrBurnIfNoAuthor(postHash, donateAmount)
        .send({
          from: userAddress,
          value: web3.utils.toWei(tipAmount, "ether"),
          gasPrice: parseInt(gasParams.gasPrice),
        })
        .on("transactionHash", (hash) => {
          setTransactionHash(hash);
          setTipStatus("Tip Transaction Created");
        });
    } else if (currentChainID == 1) {
      let donateAmount = BigInt(Number(tipAmount) * 10 ** 18 * (1 / 100));

      await contract.methods
        .tipHashOrBurnIfNoAuthor(postHash, donateAmount)
        .send({
          from: userAddress,
          value: web3.utils.toWei(tipAmount, "ether"),
        })
        .on("transactionHash", (hash) => {
          setTransactionHash(hash);
          setTipStatus("Tip Transaction Created");
        });
    } else {
      setErrorMessage("Please select either the Ethereum or xDai network.\n");
      return;
    }
  }

  useEffect(() => {
    waitForTransaction(transactionHash);
  }),
    [transactionHash];

  async function waitForTransaction(transactionH) {
    let web3 = new Web3(Web3.givenProvider);
    if (transactionH === null || transactionH === "") {
      return;
    }
    const expectedBlockTime = 1000;
    let transactionReceipt = null;
    let timeoutCounter = 0;
    while (transactionReceipt == null && timeoutCounter <= 20) {
      // Waiting expectedBlockTime until the transaction is mined
      transactionReceipt = await web3.eth.getTransactionReceipt(transactionH);
      await sleep(expectedBlockTime);

      if (transactionReceipt !== null && transactionReceipt !== "") {
        setTipStatus("Tip Successful");
        transactionReceipt = "";
        setTransactionHash(null);
      }
      timeoutCounter = timeoutCounter + 1;
    }
    if (timeoutCounter > 20) {
      setTipStatus(
        "The transaction is taking a while, check your wallet for the transaction status."
      );
      setTransactionHash(null);
    }
    transactionReceipt = null;
  }

  return (
    <TipAreaStyling>
      {mmInstalled === false && (
        <TipAreaStyling>
          <div>
            {`To send tips you must have an account on `}
            <Link href="https://metamask.io/">
              <a target="_blank">{`MetaMask`}</a>
            </Link>
            {` first.`}
            <br />
            <br />
          </div>
        </TipAreaStyling>
      )}
      {showTipForm === false && (
        <StyledButton onClick={() => connectWallet()}>Tip Author</StyledButton>
      )}

      {showTipForm === true && (
        <div>
          <strong>Amount{tipCurrency}:</strong>
          <StyledInput
            type="number"
            name="tipAmount"
            value={tipAmount}
            onChange={(event) => setTipAmount(event.target.value)}
          />

          {errorMessage + tipStatus !== "" && (
            <TipAreaStyling>
              {errorMessage}
              {tipStatus}
            </TipAreaStyling>
          )}
          <StyledButton onClick={() => sendTip()}>Send Tip</StyledButton>
        </div>
      )}
    </TipAreaStyling>
  );
}

const StyledButton = styled.button`
  width: 100%;
  font-size: 85%;
  max-width: 120px;
`;

const TipAreaStyling = styled.div`
  justify-content: center;
  align-items: center;
  text-align: center;
  padding: 0rem 0rem 0rem;
  margin: 1em 0;
  font-size: 100%;
  width: 90%;
  background: #eaeaea;
  max-width: 110px;
`;

const StyledInput = styled.input`
  type: number;
  width: 92%;
  top: 0;
  left: 0;
  padding: 0 0 1;
  font-size: 80%;
  margin: 1em 0;
`;
