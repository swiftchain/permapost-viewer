import styled from "styled-components";
import Link from "next/link";

export default function Menu() {
  return (
    <StyledUl>
      <DropDownLi>
        <Dropbtn>
          <strong>Dev Links</strong>
        </Dropbtn>
        <DropDownContent>
          <SubA
            href="https://bitbucket.org/swiftchain/permapost-viewer/"
            rel="noreferrer"
            target="_blank"
          >
            Source Code
          </SubA>
          <SubA
            href="https://etherscan.io/address/0x8D213dc5Cc6c3D2746E2B90a2D1C9E589ebCb532"
            rel="noreferrer"
            target="_blank"
          >
            Ethereum Contract
          </SubA>
          <SubA
            href="https://blockscout.com/xdai/mainnet/address/0x2024A680EE628725bd99c36Df223189f678C6945"
            rel="noreferrer"
            target="_blank"
          >
            XDai Contract
          </SubA>
        </DropDownContent>
      </DropDownLi>
    </StyledUl>
  );
}

const StyledUl = styled.ul`
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #222;
  display: inline-block;
  float: right;
  @media screen and (max-width: 40em) {
    width: 100%;
    clear: right;
  }
`;

const StyledLi = styled.li`
  float: left;
`;

const Dropbtn = styled.div`
  cursor: pointer;
  display: inline-block;
  color: white;
  text-align: center;
  padding: 0 16px;
  text-decoration: none;
  width: 7.5em;
  line-height: 3.5em;
`;

const DropDownContent = styled.div`
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  z-index: 1;
  right: 0;
  @media screen and (max-width: 40em) {
    width: 100%;
  }
`;

const DropDownLi = styled(StyledLi)`
  display: inline-block;
  &:hover {
    background-color: #333333;
  }
  &:hover ${DropDownContent} {
    display: block;
  }
  @media screen and (max-width: 40em) {
    width: 100%;
  }
`;

const SubA = styled.a`
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
  &:hover {
    background-color: #f1f1f1;
  }
  @media screen and (max-width: 40em) {
    text-align: right;
  }
`;
