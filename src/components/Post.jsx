import { useState, useEffect } from "react";

import styled from "styled-components";
import ReactMarkdown from "react-markdown";
import gfm from "remark-gfm";

import useScript from "../lib/useScript";

import Link from "next/link";

import PhaceElement from "./../components/PhaceElement";
import TipAuthor from "./TipAuthor";
import Head from "next/head";
import CopyPageLink from "./CopyPageLink";

const abiDecoder = require("abi-decoder");
const ABI = require("../lib/abi_2021_02_25.json");
abiDecoder.addABI(ABI);

import CONTRACT_ABI from "../lib/abi_2021_02_25.json";

export default function Post({ tx, className = "" }) {
  const [ethWeb3, setEthWeb3] = useState(null);
  const [xDaiWeb3, setXDaiEthWeb3] = useState(null);
  const [connectedWeb3, setConnectedEthWeb3] = useState(null);
  // txDetails
  const [methodName, setMethodName] = useState("");
  const [methodParams, setMethodParams] = useState([]);
  const [donateAmount, setDonateAmount] = useState(-1);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [postBody, setPostBody] = useState("");
  const [version, setVersion] = useState("");
  const [replyBlock, setReplyBlock] = useState("");
  const [replyHexId, setReplyHexId] = useState("");
  const [topic, setTopic] = useState("");
  const [eventName, setEventName] = useState("");
  const [eventParams, setEventParams] = useState([]);
  const [fromAddress, setFromAddress] = useState("");
  const [eventHash, setEventHash] = useState("");
  const [burnAmount, setBurnAmount] = useState("");
  const [burnUsd, setBurnUsd] = useState("");
  const [smokeSignalMessageObject, setSmokeSignalMessageObject] =
    useState(null);
  const [additionalTxEvents, setAdditionalTxEvents] = useState(false);
  const [resultBlockchainName, setResultBlockchainName] = useState(null);
  const [resultTxHash, setResultTxHash] = useState(null);

  const [fetchingblockLoading, setFetchingblockLoading] = useState(false);
  const [fetchblockData, setFetchblockData] = useState("");
  const [togglePostDetailArea, setTogglePostDetailArea] = useState(false);
  const [blockNumber, setBlockNumber] = useState(false);

  const [inputData, setInputData] = useState("");
  const [postFound, setPostFound] = useState(true);
  const [txNotFound, setTxNotFound] = useState(false);

  const [tipAmount, setTipAmount] = useState("");

  const web3LoadStatus = useScript(
    "https://cdn.jsdelivr.net/npm/web3@latest/dist/web3.min.js"
  );

  // Init the eth and xDAI web3 clients. These can be replaced with a
  // just-in-time init system
  // This useEffect actually just waits for web3LoadStatus,
  // the other dependencies are just to prevent double initialisations
  useEffect(() => {
    if (typeof window != "undefined" && web3LoadStatus === "ready") {
      if (!ethWeb3) {
        setEthWeb3(new window.Web3(process.env.ETH_RPC));
      }
      if (!xDaiWeb3) {
        setXDaiEthWeb3(new window.Web3(process.env.XDAI_RPC));
      }
    }
  }, [web3LoadStatus, ethWeb3, setEthWeb3, xDaiWeb3, setXDaiEthWeb3]);

  // Init a web3 instance with the connected wallet (like metamask)
  // This is done just-in-time if the user has a wallet connected
  useEffect(() => {
    if (tx?.blockchain_id === "connected" && !connectedWeb3) {
      console.log("Startup, test eth_requestAccounts");
      window.ethereum.send("eth_requestAccounts").then((sendTest) => {
        console.log("sendTest", sendTest);
        console.log("updating web3");
        const newConnectedEthWeb3 = new window.Web3(window.ethereum);
        console.log("newConnectedEthWeb3", newConnectedEthWeb3);
        setConnectedEthWeb3(newConnectedEthWeb3);
      });
    }
  }, [tx?.blockchain_id, connectedWeb3]);

  // Fetch the post on the provided tx hash
  // this useEffect should only run when the tx hash changes, but
  // wil be retried if the web3 instances weren't ready yet
  useEffect(() => {
    const blockChainLookup = {
      xdai: xDaiWeb3,
      eth: ethWeb3,
      connected: connectedWeb3,
    };
    setInputData("");
    setPostFound(true);
    setTxNotFound(false);
    if (
      (ethWeb3 && xDaiWeb3 && tx?.hash && tx?.blockchain_id) ||
      (tx?.blockchain_id === "connected" && connectedWeb3)
    ) {
      // use the web3 instance based on the user's selected blockchain
      const web3 = blockChainLookup?.[tx?.blockchain_id] || ethWeb3;
      console.log("tx", tx);
      console.log("tx?.hash", tx?.hash);
      // had to set this local variable here because tx?.hash became
      // undefined in the readBlockchain function
      // A better solution should be implemented here
      const txHash = tx?.hash;
      const txBlockchainName = tx?.blockchain_name || tx?.blockchain_id;
      const readBlockchain = async () => {
        try {
          setFetchingblockLoading(true);
          console.log("readBlockchain tx", tx);
          console.log("readBlockchain tx?.hash", tx?.hash);
          console.log("readBlockchain txHash", txHash);
          console.log("web3.version", web3?.version);

          // sanity check for debugging. This call should always succeed
          const latest_block = await web3.eth.getBlockNumber();
          console.log("latest_block", latest_block);
          if (!latest_block) return;

          // get the receipt for the provided transaction
          const tx_receipt = await web3?.eth?.getTransactionReceipt(txHash);
          console?.log("tx_receipt", tx_receipt);
          if (!tx_receipt) {
            setPostFound(false);
            setTxNotFound(true);
            return;
          }

          setBlockNumber(tx_receipt.blockNumber);
          const getBlockObj = await web3.eth.getBlock(tx_receipt.blockNumber);
          setFetchblockData(getBlockObj.timestamp);

          // use the abiDecoder module to decode the logs from the receipt
          const tx_logs = await abiDecoder.decodeLogs(tx_receipt?.logs);
          console?.log("tx_logs", tx_logs);

          // convenience assignments
          const tx_log_0 = tx_logs?.[0];
          console?.log("tx_log_0", tx_log_0);
          const tx_event = tx_log_0?.name || "";
          console?.log("tx_event", tx_event);

          // We currently only parse the first log. I don't know if
          // there is a use case for unpacking additional logs
          setAdditionalTxEvents(tx_logs?.length > 1);

          // display which contract event occured. For debugging purposes
          setEventName(tx_event);

          // parse the events on the tx log
          const event_params = [];
          let hash = "";
          let from = "";
          let burn_amount = 0;
          let burn_usd = 0;
          let message = null;
          tx_log_0?.events?.forEach?.((event) => {
            console.log("event", event);
            switch (event.name) {
              case "_hash": {
                hash = event.value;
                break;
              }
              case "_from": {
                from = event.value;
                break;
              }
              case "_burnAmount": {
                burn_amount = event.value;
                break;
              }
              case "_burnUsdValue": {
                burn_usd = event.value;
                break;
              }
              case "_message": {
                message = event.value;
                break;
              }
              default: {
                event_params.push({
                  name: event.name,
                  value: event.value,
                });
                break;
              }
            }
          });
          console?.log("event_params", event_params);
          console?.log("from", from);
          console?.log("hash", hash);
          console?.log("burn_amount", burn_amount);
          console?.log("burn_usd", burn_usd);
          setEventParams(event_params);
          setFromAddress(from);
          //console.log("Author Address: ", fromAddress);
          setEventHash(hash);

          // scale the burn amount to a sane magnitude
          const burn_amount_scaled = burn_amount / 10 ** 18;
          const burn_amount_formatted = burn_amount_scaled.toFixed(18);
          const burn_amount_trimmed = burn_amount_formatted.replace(
            /(\.0)?0+$/,
            `$1`
          );
          setBurnAmount(burn_amount_trimmed);
          setBurnUsd(burn_usd / 10 ** 18);
          setInputData("");
          if (!message) {
            // clear everything if we couldn't find the message
            setVersion(-1);
            setTitle("");
            setDescription("");
            setPostBody("");
            setTopic("");
            setReplyBlock("");
            setReplyHexId("");
            setSmokeSignalMessageObject(null);
          } else {
            // remove contract method call info
            const message_str = message?.slice(12);

            // parse only the smoke signal message object
            const message_json = JSON?.parse(message_str);

            // unpack smoke signal message object
            setSmokeSignalMessageObject(message_json);
            setVersion(message_json?.v);
            setTitle(message_json?.m[0]);
            setDescription(message_json?.m[1]);
            setPostBody(message_json?.m[2]);
            setTopic(message_json?.c?.topic || "");
            setReplyBlock(message_json?.c?.re?.[0] || "");
            setReplyHexId(message_json?.c?.re?.[1] || "");
          }
          if (!txHash) return;

          // get additional transaction info
          const tx = await web3?.eth?.getTransaction(txHash);
          console?.log("tx", tx);
          const tx_data_hex = tx?.input;

          if (!from) {
            setFromAddress(tx?.from);
          }

          try {
            let new_contract = null;

            if (tx?.chainId === "0x0") {
              new_contract = await new web3.eth.Contract(
                CONTRACT_ABI,
                process.env.ETH_CONTRACT_ADDRESS
              );
            } else if (tx?.chainId === "0x64") {
              new_contract = await new web3.eth.Contract(
                CONTRACT_ABI,
                process.env.XDAI_CONTRACT_ADDRESS
              );
            }

            const getTips = await new_contract.methods
              .storedMessageData(hash)
              .call();
            setTipAmount(
              parseFloat(web3?.utils?.fromWei(getTips.dollarsTipped))
            );
          } catch (error) {
            console.error("tips", error);
          }

          if (!tx_data_hex) return;
          const input_ascii = web3?.utils?.hexToUtf8(tx_data_hex);
          setInputData(input_ascii);

          // use the abiDecoder module to decode the tx data
          const tx_data_decoded = await abiDecoder.decodeMethod(tx_data_hex);
          console?.log("tx_data_decoded", tx_data_decoded);

          // unpack contract method data
          const method_name = tx_data_decoded?.name || "";
          console?.log("method_name", method_name);
          setMethodName(method_name);
          const method_param_list = tx_data_decoded?.params;
          console?.log("method_param_list", method_param_list);

          // parse contract method parameters
          const method_params = [];
          let donate_amount = 0;
          method_param_list?.forEach?.((event) => {
            switch (event.name) {
              case "donate_amount": {
                donate_amount = event.value;
                break;
              }
              case "_message": {
                break;
              }
              default: {
                method_params.push({
                  name: event.name,
                  value: event.value,
                });
                break;
              }
            }
          });

          // scale the donation amount to a sane magnitude
          const donate_amount_scaled = donate_amount / 10 ** 18;
          const donate_amount_formatted = donate_amount_scaled.toFixed(18);
          const donate_amount_trimmed = donate_amount_formatted.replace(
            /(\.0)?0+$/,
            `$1`
          );
          setDonateAmount(donate_amount_trimmed);
          setMethodParams(method_params);
          setResultBlockchainName(txBlockchainName);
          console.log("Blockchain NAME: ", txBlockchainName);
          setResultTxHash(txHash);
          setFetchingblockLoading(false);
        } catch (error) {
          setFetchingblockLoading(false);
          //setSmokeSignalMessageObject(null);
        }
      };
      try {
        setSmokeSignalMessageObject(null);

        // call the async function in this useEffect.
        // The function should be stopped when this component unmounts.
        // do this by setting a flag at the start of the useEffect and
        // then clearing the flag when the useEffect's returns
        readBlockchain();
      } catch (error) {
        setFetchingblockLoading(false);
        setSmokeSignalMessageObject(null);
      }
    }
  }, [tx, ethWeb3, xDaiWeb3, connectedWeb3]);

  // dont display anything on error
  if (!tx || !tx?.hash || !tx?.blockchain_id) {
    return null;
  }

  return (
    <PostStyling>
      <Head>
        <meta property="og:title" content={title} key="ogtitle" />
        <meta
          property="og:description"
          content={description + " " + postBody.slice(0, 200)}
          key="ogdescription"
        />
        <meta property="title" content={title} key="pagetitle" />
        <meta
          property="description"
          content={description + " " + postBody.slice(0, 200)}
          key="pagedescription"
        />
      </Head>

      {smokeSignalMessageObject && (
        <PostDetailAreaStyling
          className={togglePostDetailArea ? "show-post-detail" : ""}
        >
          <DetailStyling>
            <DetailNameStyling>Blockchain:</DetailNameStyling>
            <DetailValueStyling>{resultBlockchainName}</DetailValueStyling>
          </DetailStyling>
          <DetailStyling>
            <DetailNameStyling>Transaction Hash:</DetailNameStyling>
            <DetailValueStyling>{resultTxHash}</DetailValueStyling>
          </DetailStyling>
          <DetailStyling>
            <DetailNameStyling>Contract Event:</DetailNameStyling>
            <DetailValueStyling>{eventName}</DetailValueStyling>
          </DetailStyling>
          {!eventParams?.length ? null : (
            <ParameterStyling>
              Unknown Event Parameters:
              {eventParams.map((param, i) => {
                return (
                  <DetailStyling key={i}>
                    <DetailNameStyling>{param.name}:</DetailNameStyling>
                    <DetailValueStyling>{param.value}</DetailValueStyling>
                  </DetailStyling>
                );
              })}
            </ParameterStyling>
          )}
          {!additionalTxEvents ? null : (
            <DetailStyling>
              <NotificationStyling>
                Additional Unparsed Events Present!
              </NotificationStyling>
            </DetailStyling>
          )}
          <DetailStyling>
            <DetailNameStyling>Contract Method:</DetailNameStyling>
            <DetailValueStyling>{methodName}</DetailValueStyling>
          </DetailStyling>
          {!eventParams?.length ? null : (
            <ParameterStyling>
              Unknown Method Parameters:
              {methodParams.map((param, i) => {
                return (
                  <DetailStyling key={i}>
                    <DetailNameStyling>{param.name}:</DetailNameStyling>
                    <DetailValueStyling>{param.value}</DetailValueStyling>
                  </DetailStyling>
                );
              })}
            </ParameterStyling>
          )}
          <DetailStyling>
            <DetailNameStyling>From:</DetailNameStyling>
            <DetailValueStyling>{fromAddress}</DetailValueStyling>
          </DetailStyling>
          <DetailStyling>
            <DetailNameStyling>Hash:</DetailNameStyling>
            <DetailValueStyling>{eventHash}</DetailValueStyling>
          </DetailStyling>
          <DetailStyling>
            <DetailNameStyling>Donate Amount:</DetailNameStyling>
            <DetailValueStyling>{donateAmount}</DetailValueStyling>
          </DetailStyling>
          <DetailStyling>
            <DetailNameStyling>Burn Amount:</DetailNameStyling>
            <DetailValueStyling>{burnAmount}</DetailValueStyling>
          </DetailStyling>
          <DetailStyling>
            <DetailNameStyling>Burn USD:</DetailNameStyling>
            <DetailValueStyling>
              {Math.round(burnUsd * 100) / 100}
            </DetailValueStyling>
          </DetailStyling>
          {!version ? null : (
            <DetailStyling>
              <DetailNameStyling>SmokeSignal version:</DetailNameStyling>
              <DetailValueStyling>{version}</DetailValueStyling>
            </DetailStyling>
          )}
          {resultBlockchainName === "xDai" ? (
            <Link
              href={`https://blockscout.com/xdai/mainnet/tx/${resultTxHash}`}
            >
              <a target="_blank" rel="noreferrer">
                View on block explorer
              </a>
            </Link>
          ) : (
            <Link href={`https://etherscan.io/tx/${resultTxHash}`}>
              <a target="_blank" rel="noreferrer">
                View on block explorer
              </a>
            </Link>
          )}
          <button onClick={() => setTogglePostDetailArea(false)}>Close</button>
        </PostDetailAreaStyling>
      )}
      <FlexWrapper>
        <LeftCol>
          {smokeSignalMessageObject && (
            <StickyElement>
              <PhaceComponent>
                <Link href={`/author/${fromAddress}`}>
                  <a>
                    <PhaceElement messageFlag={fromAddress} />
                  </a>
                </Link>
              </PhaceComponent>

              <div>{new Date(fetchblockData * 1000).toDateString()}</div>
              <div>
                <Link href={`/author/${fromAddress}`}>
                  <a>Author</a>
                </Link>
              </div>
              {smokeSignalMessageObject && (
                <div className="hidden-xs">
                  <ViewblockExplorerButton
                    onClick={() => setTogglePostDetailArea(true)}
                  >
                    View block
                  </ViewblockExplorerButton>
                </div>
              )}

              <div className="hidden-xs">
                <Link
                  href={`https://smokesignal.zone/#!/post?block=${blockNumber}&hash=${eventHash}`}
                >
                  <a target="_blank" rel="noreferrer">
                    Link to SS
                  </a>
                </Link>
              </div>
              <TipsReceived title="Tips received">
                {tipAmount >= 0.01 ? (
                  <span>
                    <strong>$</strong>
                    {tipAmount.toFixed(2)}
                  </span>
                ) : (
                  <span>{tipAmount === "" ? `...` : `No tips.`}</span>
                )}
              </TipsReceived>
              <TipAuthor
                postHash={eventHash}
                blockChainName={resultBlockchainName}
              />
              <CopyPageLink />
            </StickyElement>
          )}

          {inputData && !smokeSignalMessageObject && (
            <StickyElement>
              <PhaceComponent>
                <Link href={`/author/${fromAddress}`}>
                  <a>
                    <PhaceElement messageFlag={fromAddress} />
                  </a>
                </Link>
              </PhaceComponent>

              <div>{new Date(fetchblockData * 1000).toDateString()}</div>
              <div>
                <Link href={`/author/${fromAddress}`}>
                  <a>Author</a>
                </Link>
              </div>

              <CopyPageLink />
            </StickyElement>
          )}
        </LeftCol>

        <PostContentAreaStyling>
          {smokeSignalMessageObject ? (
            <>
              <PostTitleStyling>{title}</PostTitleStyling>
              <PostBodyStyling>
                <ReactMarkdown remarkPlugins={[gfm]}>{postBody}</ReactMarkdown>
              </PostBodyStyling>
              <VisibleOnMobileOnly>
                {smokeSignalMessageObject && (
                  <div>
                    <ViewblockExplorerButton
                      onClick={() => setTogglePostDetailArea(true)}
                    >
                      View block
                    </ViewblockExplorerButton>
                  </div>
                )}
                <div>
                  <Link
                    href={`https://smokesignal.zone/#!/post?block=${blockNumber}&hash=${eventHash}`}
                  >
                    <a target="_blank" rel="noreferrer">
                      Link to SmokeSignal
                    </a>
                  </Link>
                </div>
              </VisibleOnMobileOnly>
              <StyledDiscussSS>
                <a
                  target="_blank"
                  rel="noreferrer"
                  href={`https://smokesignal.zone/#!/post?block=${blockNumber}&hash=${eventHash}`}
                >
                  Discuss this on SmokeSignal
                </a>
              </StyledDiscussSS>
            </>
          ) : eventName === "HashTip" ? (
            <DetailStyling>
              <NotificationStyling>{`#Tips don't have bodies#`}</NotificationStyling>
            </DetailStyling>
          ) : (
            <DetailStyling>
              <NotificationStyling>
                {fetchingblockLoading && postFound ? (
                  <p>Fetching SmokeSignal object ...</p>
                ) : (
                  <LightHeaderStyle>
                    No SmokeSignal object found.
                    {txNotFound && <p>(Transaction not found)</p>}
                  </LightHeaderStyle>
                )}

                {inputData && !fetchingblockLoading && postBody == "" ? (
                  <div>
                    <LightHeaderStyle>
                      <p>
                        But the transaction appears to be UTF-8 encoded text!
                        Here it is:
                      </p>
                    </LightHeaderStyle>
                    <p>
                      <ReactMarkdown remarkPlugins={[gfm]}>
                        {inputData}
                      </ReactMarkdown>
                    </p>
                  </div>
                ) : (
                  ""
                )}
              </NotificationStyling>
            </DetailStyling>
          )}
        </PostContentAreaStyling>
      </FlexWrapper>
    </PostStyling>
  );
}

const TipsReceived = styled.div`
  background: url(/piggy-bank.svg) no-repeat center left;
  padding-left: 1.5em;
  background-size: 25px;
  text-align: left;
  color: rgba(41, 41, 41, 1);
  width: unset;
  font-size: 1em;
  font-weight: normal;
  color: rgba(41, 41, 41, 1);
  margin-top: 1em;
  margin-bottom: 1.5em;
  strong {
    margin-right: 0.2em;
  }
`;

const StyledDiscussSS = styled.div`
  text-align: center;
  a {
    display: block;
    padding: 1em;
    background: #eaeaea;
    border-radius: 6px;
    text-align: center;
    margin-top: 1rem;
    margin-bottom: 2rem;
    display: inline-block;
  }
`;

const PhaceComponent = styled.div`
  margin-bottom: 1em;
  svg {
    border-radius: 5px;
  }
`;

const FlexWrapper = styled.div`
  display: flex;
  > div {
    flex: 0 0 1;
    position: relative;
  }
  @media screen and (max-width: 650px) {
    display: block;
  }
`;

const LeftCol = styled.div`
  flex: 0 0 130px;
`;

const VisibleOnMobileOnly = styled.div`
  display: none;
  @media screen and (max-width: 650px) {
    display: block;
  }
`;

const StickyElement = styled.div`
  position: sticky;
  top: 4.5em;
`;

const ViewblockExplorerButton = styled.button`
  min-height: auto;
  background: #fff;
  box-shadow: none;
  color: #000;
  padding: 0;
  margin: 0.4em 0;
  text-decoration: underline;
  text-align: left;
  &:hover {
    box-shadow: none;
  }
`;

const PostStyling = styled.div`
  border: none;
  padding: 2em 0 0;
`;

const DetailStyling = styled.div`
  display: flex;
  flex-direction: row;
  margin: 0 0 0.5rem 0;
  word-break: break-word;
`;
const DetailNameStyling = styled.div``;
const DetailValueStyling = styled.div`
  font-weight: bold;
  padding-left: 0.3571em;
`;
const ParameterStyling = styled.div``;
const NotificationStyling = styled.div``;

const PostDetailAreaStyling = styled.div`
  border-radius: 0.2142em;
  background: #ffffff;
  padding: 1rem 2rem;
  margin-top: 3rem;
  font-size: 0.8571em;
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translateX(-50%) translateY(-50%);
  box-shadow: 0 0 300px;
  display: none;
  width: 96%;
  max-width: 650px;
  z-index: 5;
  &.show-post-detail {
    display: block;
  }
`;
const PostContentAreaStyling = styled.div`
  border: none;
  padding-top: 0;
  margin-top: 0;
  h1 {
    margin-top: 0;
  }
`;
const PostTopicStyling = styled.div`
  font-weight: 400;
  text-decoration: none;
  border-radius: 0.2142em;
  font-size: 0.9285em;
  color: rgba(0, 0, 0, 0.87);
  line-height: 1.5714em;
  padding: 0.3571em 0.7142em;
  background: #eaeaea;
  display: inline-flex;
  margin-top: 1rem;
  margin-bottom: 1rem;
`;
const PostTitleStyling = styled.h1`
  font-family: Georgia, Cambria, "Times New Roman", Times, serif;
  font-weight: normal;
`;

const LightHeaderStyle = styled.div`
  background: none;
  background-image: url(/info-circle-solid.svg);
  background-repeat: no-repeat;
  background-position: left top;
  margin-bottom: 1rem;
  padding: 0 1rem 0 2rem;
  border-radius: 5px;
  color: #555;
  text-align: left;
  font-weight: normal;
  font-size: 1rem;
  background-size: 24px;
  min-height: 24px;
  p {
    margin: 0;
  }
`;

const PostBodyStyling = styled.div`
  font-family: Georgia, Cambria, "Times New Roman", Times, serif;
  a {
    font-weight: normal;
  }
  h1 {
    margin-bottom: 1em;
    line-height: 1.65em;
  }
  h2,
  h3,
  h4,
  h5,
  h6 {
    font-family: Lato, "Helvetica Neue", Arial, Helvetica, sans-serif;
    margin-bottom: 1.5em;
  }
  p {
    text-size-adjust: 100%;
    line-height: 1.6285em;
    margin: 0px 0em 2.05em;
    letter-spacing: -0.003em;
  }
  ul,
  ol {
    list-style: inherit;
    margin-bottom: 1em;
    li {
      margin-left: 1.2em;
    }
  }
  i,
  em {
    font-style: italic;
  }
`;
