import { useState } from "react";
import styled from "styled-components";
import Modal from "./Modal";

const FirstTimeMessages = ({
  showMessages,
  setShowMessages,
  welcomeWalletAddress = "(address here)",
  acceptTnCs,
}) => {
  const [modal1Visible, setModal1Visible] = useState(true);
  const [modal2Visible, setModal2Visible] = useState(false);
  const [modal3Visible, setModal3Visible] = useState(false);
  const [modal4Visible, setModal4Visible] = useState(false);
  const [modal5Visible, setModal5Visible] = useState(false);
  const [modal6Visible, setModal6Visible] = useState(false);

  let firstTimeMessages = {
    message1: (
      <MessageAreaStyling>
        {`Welcome ${welcomeWalletAddress}, we haven’t seen you post on here before.
          Since we don’t know who you are by name, when we refer to “you” in any
          text, we’re referring to ${welcomeWalletAddress}. We really appreciate you
          having a look around and hope you find great value in Permapost.
          Before you proceed, you should know that posting on the blockchain
          comes with a few warnings. We want to make sure you are aware of these
          warnings.`}
      </MessageAreaStyling>
    ),
    message2: (
      <MessageAreaStyling>
        {`If you are familiar with how the blockchain works, you’ll know that
          any transaction on the blockchain is permanent, and can never be
          modified or removed unless the blockchain it’s on stops working or is
          taken down somehow. This is very improbable and will most likely not
          happen.`}
      </MessageAreaStyling>
    ),
    messaeg3: (
      <MessageAreaStyling>
        {`That being said, we don’t want to scare you away, it’s just important
          that you’re aware of the risks. Firstly, since our platform is
          decentralised, unregulated, public and unkillable, you will have to
          understand that whatever you post on here can never be removed or
          deleted in any way. Although an interface like Permapost could one day
          hide posts, they will still persist as public data on the blockchain.`}
      </MessageAreaStyling>
    ),
    message4: (
      <MessageAreaStyling>
        {`We would like to take the opportunity to ask that you accept the risks
          of posting here:`}
        <ul>
          <li>
            {`${"\u2022"} Our content is public and available for all to see.`}
          </li>
          <li>{`${"\u2022"} Your posts will be stored on a blockchain.`}</li>
          <li>
            {`${"\u2022"} Your post will create a transaction on the blockchain
              and its content will be stored inside the transaction’s data.`}
          </li>
          <li>
            {`${"\u2022"} Your post is permanent and cannot be removed or
              modified.`}
          </li>
          <li>{`${"\u2022"} You will be known only as ${welcomeWalletAddress}.`}</li>
          <li>
            {`${"\u2022"} Unless you make yourself publicly known by stating your
              name on the post, no additional information will be stored in
              terms of your identity.`}
          </li>
          <li>
            {`${"\u2022"} You should know that, ultimately, there may be ways to
              track the wallet address back to you, although this is very
              difficult and there are no easy ways to perform this action.`}
          </li>
          <li>
            {`${"\u2022"} The fact that you accepted these terms will be stored
              as your first post on the blockchain, as proof that we made you
              aware of the risks.`}
          </li>
        </ul>
      </MessageAreaStyling>
    ),
    message5: (
      <MessageAreaStyling>
        {`The developers of Permapost reserve the right to deploy future
          versions of the platform that hides your content from the public eye.
          We resist this move ideologically but may face legal or other pressure
          that forces our hand.`}
      </MessageAreaStyling>
    ),
    message6: (
      <MessageAreaStyling>
        {`Okay, ${welcomeWalletAddress}, if you accept these terms and conditions you’ll be free to post on Permapost.
          Happy posting!`}
      </MessageAreaStyling>
    ),
  };

  return (
    showMessages && (
      <div>
        <Modal show={modal1Visible} hideModal={() => setShowMessages(false)}>
          <MessageTitleAreaStyling>
            <h2>Welcome</h2>
          </MessageTitleAreaStyling>
          <div>{firstTimeMessages.message1}</div>
          <MessageButtonStyling>
            <button
              onClick={() => {
                setModal1Visible(false);
                setModal2Visible(true);
              }}
            >
              Next
            </button>
          </MessageButtonStyling>
        </Modal>
        <Modal
          show={modal2Visible}
          hideModal={() => {
            setModal2Visible(false);
            setModal1Visible(true);
            setShowMessages(false);
          }}
        >
          <MessageTitleAreaStyling>
            <h2>Welcome</h2>
          </MessageTitleAreaStyling>
          <div>{firstTimeMessages.message2}</div>
          <MessageButtonStyling>
            <button
              onClick={() => {
                setModal2Visible(false);
                setModal3Visible(true);
              }}
            >
              Next
            </button>
          </MessageButtonStyling>
        </Modal>
        <Modal
          show={modal3Visible}
          hideModal={() => {
            setModal3Visible(false);
            setModal1Visible(true);
            setShowMessages(false);
          }}
        >
          <MessageTitleAreaStyling>
            <h2>Welcome</h2>
          </MessageTitleAreaStyling>
          <div>{firstTimeMessages.messaeg3}</div>
          <MessageButtonStyling>
            <button
              onClick={() => {
                setModal3Visible(false);
                setModal4Visible(true);
              }}
            >
              Next
            </button>
          </MessageButtonStyling>
        </Modal>
        <Modal
          show={modal4Visible}
          hideModal={() => {
            setModal4Visible(false);
            setModal1Visible(true);
            setShowMessages(false);
          }}
        >
          <MessageTitleAreaStyling>
            <h2>Welcome</h2>
          </MessageTitleAreaStyling>
          <div>{firstTimeMessages.message4}</div>
          <MessageButtonStyling>
            <button
              onClick={() => {
                setModal4Visible(false);
                setModal5Visible(true);
              }}
            >
              Next
            </button>
          </MessageButtonStyling>
        </Modal>
        <Modal
          show={modal5Visible}
          hideModal={() => {
            setModal5Visible(false);
            setModal1Visible(true);
            setShowMessages(false);
          }}
        >
          <MessageTitleAreaStyling>
            <h2>Welcome</h2>
          </MessageTitleAreaStyling>
          <div>{firstTimeMessages.message5}</div>
          <MessageButtonStyling>
            <button
              onClick={() => {
                setModal5Visible(false);
                setModal6Visible(true);
              }}
            >
              Next
            </button>
          </MessageButtonStyling>
        </Modal>
        <Modal
          show={modal6Visible}
          hideModal={() => {
            setModal6Visible(false);
            setModal1Visible(true);
            setShowMessages(false);
          }}
        >
          <MessageTitleAreaStyling>
            <h2>Welcome</h2>
          </MessageTitleAreaStyling>
          <div>{firstTimeMessages.message6}</div>
          <MessageButtonStyling>
            <button
              onClick={() => {
                acceptTnCs();
                //createPost();
                setModal6Visible(false);
                setModal1Visible(true);
              }}
            >
              Accept
            </button>
          </MessageButtonStyling>
        </Modal>
      </div>
    )
  );
};

export default FirstTimeMessages;

const MessageAreaStyling = styled.div`
  padding: 1rem 1.5rem;
  min-height: 300px;
  min-width: 300px;
  font-size: 120%;
  width: 100%;
  border-bottom: 0.07142em solid #eaeaea;
  top: 0;
  left: 0;
  background: #ffffff;
  word-wrap: break-word;
`;

const MessageTitleAreaStyling = styled.div`
  width: 100%;
  border-bottom: none;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 0;
  left: 0;
  background: #ffffff;
`;

const MessageButtonStyling = styled.div`
  padding: 0;
  display: block;
  flex-direction: column;
  justify-content: right;
  align-items: right;
  button {
    margin: auto;
    &.blue-btn {
      box-shadow: inset 0 0 0 35.7142em rgb(46, 41, 66);
      &:hover {
        background: #ffffff;
        box-shadow: inset 0 0 0 0.142em rgb(46, 41, 66);
        color: #1b1c1d;
      }
    }
  }
  .text-center {
    text-align: center;
  }
`;
