import styled from "styled-components";
import { useState } from "react";

export default function CopyPageLink() {
  const [copyButtonText, setCopyButtonText] = useState("Share Post");

  function copy() {
    const el = document.createElement("input");
    el.value = window.location.href;
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
    setCopyButtonText("Link Copied!");
    setTimeout(function () {
      setCopyButtonText("Share Post");
    }, 2000);
  }
  return (
    <CopyAreaStyling>
      <StyledButton onClick={() => copy()}>{copyButtonText}</StyledButton>
    </CopyAreaStyling>
  );
}
const StyledButton = styled.button`
  width: 100%;
  font-size: 85%;
  max-width: 120px;
`;

const CopyAreaStyling = styled.div`
  justify-content: center;
  align-items: center;
  text-align: center;
  padding: 0rem 0rem 0rem;
  margin: 1em 0;
  font-size: 100%;
  width: 90%;
  background: #eaeaea;
  max-width: 110px;
`;
